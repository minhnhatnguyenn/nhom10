﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nhom10.Binh_s_Forms
{
    public partial class frmNhanvien : Form
    {
        DataTable tblNhanvien;
        public frmNhanvien()
        {
            InitializeComponent();
        }

        private void frmNhanvien_Load(object sender, EventArgs e)
        {
            btnluu.Enabled = false;
            btnboqua.Enabled = false;
            txtmanv.Enabled = false;
            Functions.Fillcombo("SELECT Maca FROM tblcalam", cbomaca, "Maca", "Maca");
            cbomaca.SelectedIndex = -1;
            Load_DataGridView();
        }
        private void Load_DataGridView()
        {
            string sql;
            sql = "SELECT Manv,Tennv,Maca,Namsinh,Gioitinh,Diachi,Dienthoai FROM tblNhanvien";
            tblNhanvien = Functions.Getdatatotable(sql);
            dgridnhanvien.DataSource = tblNhanvien;
            dgridnhanvien.Columns[0].HeaderText = "Mã nhân viên";
            dgridnhanvien.Columns[1].HeaderText = "Tên nhân viên";
            dgridnhanvien.Columns[2].HeaderText = "Mã ca";
            dgridnhanvien.Columns[3].HeaderText = "Năm sinh";
            dgridnhanvien.Columns[4].HeaderText = "Giới tính";
            dgridnhanvien.Columns[5].HeaderText = "Địa chỉ";
            dgridnhanvien.Columns[6].HeaderText = "Điện thoại";
            dgridnhanvien.AllowUserToAddRows = false;
            dgridnhanvien.EditMode = DataGridViewEditMode.EditProgrammatically;
        }

        private void dgridnhanvien_Click(object sender, EventArgs e)
        {
            string ma;
            if (btnthem.Enabled == false)
            {
                MessageBox.Show("Đang ở chế độ thêm mới!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtmanv.Focus();
                return;
            }
            if (tblNhanvien.Rows.Count == 0)
            {
                MessageBox.Show("Không có dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (dgridnhanvien.CurrentRow.Cells["Gioitinh"].Value.ToString() == "Nam")
                chkgt.Checked = true;
            else
                chkgt.Checked = false;

            txtmanv.Text = dgridnhanvien.CurrentRow.Cells["Manv"].Value.ToString();
            txttennv.Text = dgridnhanvien.CurrentRow.Cells["Tennv"].Value.ToString();
            ma = dgridnhanvien.CurrentRow.Cells["Maca"].Value.ToString();
            cbomaca.Text = Functions.GetFieldValues("SELECT Maca FROM tblcalam WHERE Maca =N'" + ma + "'");
            txtnamsinh.Text = dgridnhanvien.CurrentRow.Cells["Namsinh"].Value.ToString();
            txtdiachi.Text = dgridnhanvien.CurrentRow.Cells["Diachi"].Value.ToString();
            mskdienthoai.Text = dgridnhanvien.CurrentRow.Cells["Dienthoai"].Value.ToString();
            btnboqua.Enabled = true;
            btnsua.Enabled = true;
            btnxoa.Enabled = true;
        }
        private void resetvalues()
        {
            txtmanv.Text = "";
            txttennv.Text = "";
            txtnamsinh.Text = "";
            txtdiachi.Text = "";
            cbomaca.Text = "";
            mskdienthoai.Text = "";
            chkgt.Checked = false;
        }

        private void btnthem_Click(object sender, EventArgs e)
        {
            txtmanv.Focus();
            btnluu.Enabled = true;
            btnboqua.Enabled = true;
            btnxoa.Enabled = false;
            btnsua.Enabled = false;
            btnthem.Enabled = false;
            resetvalues();
            txtmanv.Enabled = true;
        }

        private void btnluu_Click(object sender, EventArgs e)
        {
            string sql, gt;
            if (txtmanv.Text == "")
            {
                MessageBox.Show("Bạn phải nhập mã nhân viên", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtmanv.Focus();
                return;
            }
            if (txttennv.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên nhân viên", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txttennv.Focus();
                return;
            }
            if (txtdiachi.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập địa chỉ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtdiachi.Focus();
                return;
            }
            if (mskdienthoai.Text == "( )  ")
            {
                MessageBox.Show("Bạn phải nhập số điện thoại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                mskdienthoai.Focus();
                return;
            }
            if (txtnamsinh.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập năm sinh", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtnamsinh.Focus();
                return;

            }
            if (cbomaca.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mã ca", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cbomaca.Focus();
                return;
            }
            if (chkgt.Checked == true)
                gt = "Nam";
            else
                gt = "Nữ";
            sql = "SELECT Manv FROM tblNhanvien WHERE Manv=N'" + txtmanv.Text.Trim() + "'";
            if (Functions.CheckKey(sql))
            {
                MessageBox.Show("Mã nhân viên này đã có, bạn phải nhập mã khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtmanv.Focus();
                txtmanv.Text = "";
                return;
            }
            sql = "INSERT INTO tblNhanvien(Manv,Tennv,Maca,Namsinh,Gioitinh,Diachi,Dienthoai) VALUES(N'" + txtmanv.Text.Trim() + "',N'" + txttennv.Text.Trim() + "',N'" + cbomaca.SelectedValue.ToString() + "','" + txtnamsinh.Text.Trim() + "',N'" + gt + "',N'" + txtdiachi.Text.Trim() + "','" + mskdienthoai.Text + "')";
            Functions.RunSql(sql);
            Load_DataGridView();
            resetvalues();
            btnxoa.Enabled = true;
            btnsua.Enabled = true;
            btnluu.Enabled = false;
            btnboqua.Enabled = false;
            btnthem.Enabled = true;
            txtmanv.Enabled = false;
        }

        private void btnxoa_Click(object sender, EventArgs e)
        {
            string sql;
            if (tblNhanvien.Rows.Count == 0)
            {
                MessageBox.Show("Không có dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtmanv.Text == "")
            {
                MessageBox.Show("Bạn chưa chọn bản ghi nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (MessageBox.Show("Bạn có muốn xóa không", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                sql = "DELETE tblNhanvien WHERE Manv=N'" + txtmanv.Text + "'";
                Functions.RunSqlDel(sql);
                Load_DataGridView();
                resetvalues();
                return;
            }
        }

        private void btnsua_Click(object sender, EventArgs e)
        {
            string sql, gt;
            if (tblNhanvien.Rows.Count == 0)
            {
                MessageBox.Show("Không có dữ liệu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtmanv.Text == "")
            {
                MessageBox.Show("Bạn chưa chọn bản ghi nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txttennv.Text.Trim() == "")
            {
                MessageBox.Show("Bạn phải nhập tên khách", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txttennv.Focus();
                return;
            }
            if (cbomaca.Text.Trim() == "")
            {
                MessageBox.Show("Bạn phải nhập mã ca", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cbomaca.Focus();
                return;
            }
            if (txtdiachi.Text.Trim() == "")
            {
                MessageBox.Show("Bạn phải nhập địa chỉ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtdiachi.Focus();
                return;
            }
            if (mskdienthoai.Text == "(  )     ")
            {
                MessageBox.Show("Bạn phải nhập số điện thoại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                mskdienthoai.Focus();
                return;
            }
            if (txtnamsinh.Text.Trim() == "")
            {
                MessageBox.Show("Bạn phải nhập năm sinh", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtnamsinh.Focus();
                return;
            }
            if (chkgt.Checked == true)
                gt = "Nam";
            else
                gt = "Nữ";
            sql = "UPDATE tblNhanvien SET Tennv =N'" + txttennv.Text.Trim().ToString() + "',Diachi=N'" + txtdiachi.Text.Trim().ToString() + "',Dienthoai='" + mskdienthoai.Text.ToString() + "',Maca=N'" + cbomaca.SelectedValue.ToString() + "',Gioitinh=N'" + gt + "',Namsinh='" + txtnamsinh.Text.ToString() + "'Where Manv=N'" + txtmanv.Text + "'";
            Functions.RunSql(sql);
            Load_DataGridView();
            resetvalues();
            btnboqua.Enabled = false;
        }

        private void btnboqua_Click(object sender, EventArgs e)
        {
            resetvalues();
            btnboqua.Enabled = false;
            btnthem.Enabled = true;
            btnxoa.Enabled = true;
            btnsua.Enabled = true;
            btnluu.Enabled = false;
            txtmanv.Enabled = false;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

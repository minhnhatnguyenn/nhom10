﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace nhom10.Binh_s_Forms
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void thuêMáyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Minh_s_Form.frmThuemay f = new Minh_s_Form.frmThuemay();
            f.Show();
        }

        private void bảoTrìToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Minh_s_Form.frmBaotri f = new Minh_s_Form.frmBaotri();
            f.Show();
        }

        private void tínhTiềnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Minh_s_Form.frmTinhtien f = new Minh_s_Form.frmTinhtien();
            f.Show();
        }

        private void máyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTimkiemmay f = new frmTimkiemmay();
            f.Show();
        }

        private void nhânViênToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTimkiemnhanvien  f = new frmTimkiemnhanvien();
            f.Show();
        }

        private void phòngToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tienform.frmPhong f = new Tienform.frmPhong();
            f.Show();
        }

        private void nhânViênToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmNhanvien f = new frmNhanvien();
            f.Show();
        }

        private void máyTínhToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tienform.frmMaytinh f = new Tienform.frmMaytinh();
            f.Show();
        }

        private void chipToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tienform.frmChip f = new Tienform.frmChip();
            f.Show();
        }

        private void rAMToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRam f = new frmRam();
            f.Show();
        }

        private void ổCứngToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmOcung f = new frmOcung();
            f.Show();
        }

        private void mànHìnhToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmManhinh f = new frmManhinh();
            f.Show();
        }

        private void loaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmLoa f = new frmLoa();
            f.Show();
        }

        private void chuộtToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmChuot f = new frmChuot();
            f.Show();
        }

        private void bànPhímToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tienform.frmBanphim f = new Tienform.frmBanphim();
            f.Show();
        }

        private void nhàBảoTrìToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Minh_s_Form.frmNhabaotri f = new Minh_s_Form.frmNhabaotri();
            f.Show();
        }

        private void nguyênNhânToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Minh_s_Form.frmNguyennhan f = new Minh_s_Form.frmNguyennhan();
            f.Show();
        }

        private void giảiPhápToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Minh_s_Form.frmGiaiphap f = new Minh_s_Form.frmGiaiphap();
            f.Show();
        }

        private void phíBảoTrìToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Oanh_s_Forms.frmBaocaocpbt f = new Oanh_s_Forms.frmBaocaocpbt();
            f.Show();
        }

        private void tiềnThuêMáyToolStripMenuItem_Click(object sender, EventArgs e)
        {
             Oanh_s_Forms.frmBaocaotienthue f = new Oanh_s_Forms.frmBaocaotienthue();
             f.Show();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            PictureBox a = new PictureBox();
            a.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog a = new OpenFileDialog();
            a.Filter = "BitMap|*.bmp|Gift|*.gif|Toan bo|*.*";
            a.InitialDirectory = "D:\\";
            a.FilterIndex = 3;
            a.Title = "Hãy chọn hình ảnh để hiển thị!";
            if (a.ShowDialog() == DialogResult.OK)
                picAnh.Image = Image.FromFile(a.FileName);
            else
                MessageBox.Show("Bạn bấm Cancel rồi mà");
        }

        private void thoátToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmMenu_Load(object sender, EventArgs e)
        {
            Functions.ketnoi();
         
        }
    }
}

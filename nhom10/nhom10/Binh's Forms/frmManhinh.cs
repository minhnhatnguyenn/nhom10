﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nhom10.Binh_s_Forms
{
    public partial class frmManhinh : Form
    {
        DataTable tblManhinh;
        public frmManhinh()
        {
            InitializeComponent();
        }

        private void frmManhinh_Load(object sender, EventArgs e)
        {
            //Functions.ketnoi();
            txtmaman.Enabled = false;
            btnluu.Enabled = false;
            btnboqua.Enabled = false;
            Load_DataGridView();
        }
        private void Load_DataGridView()
        {
            string sql;
            sql = "SELECT mamanhinh, tenmanhinh, coman FROM tblManhinh";
            tblManhinh = Functions.Getdatatotable(sql);
            dgridmanhinh.DataSource = tblManhinh;
            dgridmanhinh.Columns[0].HeaderText = "Mã màn hình ";
            dgridmanhinh.Columns[1].HeaderText = "Tên màn hình";
            dgridmanhinh.Columns[2].HeaderText = "Cỡ màn hình (inch)";
            // Không cho phép thêm mới dữ liệu trực tiếp trên lưới
            dgridmanhinh.AllowUserToAddRows = false;
            // Không cho phép sửa dữ liệu trực tiếp trên lưới
            dgridmanhinh.EditMode = DataGridViewEditMode.EditProgrammatically;
        }

        private void dgridmanhinh_Click(object sender, EventArgs e)
        {
            if (btnthem.Enabled == false)
            {
                MessageBox.Show("Đang ở chế độ thêm mới!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtmaman.Focus();
                return;
            }
            if (tblManhinh.Rows.Count == 0)
            {
                MessageBox.Show("Không có dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            txtmaman.Text = dgridmanhinh.CurrentRow.Cells["mamanhinh"].Value.ToString();
            txttenman.Text = dgridmanhinh.CurrentRow.Cells["tenmanhinh"].Value.ToString();
            txtcoman.Text = dgridmanhinh.CurrentRow.Cells["coman"].Value.ToString();
            btnsua.Enabled = true;
            btnxoa.Enabled = true;
            btnboqua.Enabled = true;
        }
        private void ResetValues()
        {
            txtmaman.Text = "";
            txttenman.Text = "";
            txtcoman.Text = "";
        }

        private void btnthem_Click(object sender, EventArgs e)
        {
            btnsua.Enabled = false;
            btnxoa.Enabled = false;
            btnboqua.Enabled = true;
            btnluu.Enabled = true;
            btnthem.Enabled = false;
            ResetValues();
            txtmaman.Enabled = true;
            txtmaman.Focus();

        }

        private void btnluu_Click(object sender, EventArgs e)
        {
            string sql;
            if (txtmaman.Text == "")
            {
                MessageBox.Show("Bạn phải nhập mã màn hình ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtmaman.Focus();
                return;
            }
            if (txttenman.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên màn hình", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txttenman.Focus();
                return;
            }
            if (txtcoman.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập cỡ màn", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtcoman.Focus();
                return;
            }
            sql = "SELECT mamanhinh FROM tblManhinh WHERE mamanhinh=N'" + txtmaman.Text.Trim() + "'";
            if (Functions.CheckKey(sql))
            {
                MessageBox.Show("Mã màn hình này đã có, bạn phải nhập mã khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtmaman.Focus();
                txtmaman.Text = "";
                return;
            }
            sql = "INSERT INTO tblManhinh(mamanhinh,tenmanhinh,coman) VALUES(N'" + txtmaman.Text + "',N'" + txttenman.Text + "',N'" + txtcoman.Text + "')";
            Functions.RunSql(sql);
            Load_DataGridView();
            ResetValues();
            btnxoa.Enabled = true;
            btnthem.Enabled = true;
            btnsua.Enabled = true;
            btnboqua.Enabled = false;
            btnluu.Enabled = false;
            txtmaman.Enabled = false;
        }

        private void btnsua_Click(object sender, EventArgs e)
        {
            string sql;
            if (tblManhinh.Rows.Count == 0)
            {
                MessageBox.Show("Không còn dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtmaman.Text == "")
            {
                MessageBox.Show("Bạn chưa chọn bản ghi nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txttenman.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên màn hình ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txttenman.Focus();
                return;
            }
            if (txtcoman.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập cỡ màn ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtcoman.Focus();
                return;
            }
            sql = "UPDATE tblManhinh SET  tenmanhinh=N'" + txttenman.Text.Trim().ToString() + "',coman=N'" + txtcoman.Text.Trim().ToString() + "' WHERE mamanhinh=N'" + txtmaman.Text + "'";
            Functions.RunSql(sql);
            Load_DataGridView();
            ResetValues();
            btnboqua.Enabled = false;
        }

        private void btnxoa_Click(object sender, EventArgs e)
        {
            string sql;
            if (tblManhinh.Rows.Count == 0)
            {
                MessageBox.Show("Không còn dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtmaman.Text == "")
            {
                MessageBox.Show("Bạn chưa chọn bản ghi nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (MessageBox.Show("Bạn có muốn xóa không?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                sql = "DELETE tblManhinh WHERE mamanhinh=N'" + txtmaman.Text + "'";
                Functions.RunSqlDel(sql);
                Load_DataGridView();
                ResetValues();
            }
        }

        private void btnboqua_Click(object sender, EventArgs e)
        {
            ResetValues();
            btnboqua.Enabled = false;
            btnthem.Enabled = true;
            btnxoa.Enabled = true;
            btnsua.Enabled = true;
            btnluu.Enabled = false;
            txtmaman.Enabled = false;
        }

        private void btnthoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

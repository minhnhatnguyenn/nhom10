﻿
namespace nhom10.Binh_s_Forms
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.danhMụcToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thuêMáyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bảoTrìToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tínhTiềnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tìmKiếmToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.máyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nhânViênToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dữLiệuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.phòngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nhânViênToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.máyTínhToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.phầnCứngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chipToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rAMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ổCứngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mànHìnhToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chuộtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bànPhímToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bảoTrìToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.nhàBảoTrìToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nguyênNhânToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.giảiPhápToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.báoCáoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.phíBảoTrìToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tiềnThuêMáyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thoátToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.picAnh = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAnh)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.danhMụcToolStripMenuItem,
            this.tìmKiếmToolStripMenuItem,
            this.dữLiệuToolStripMenuItem,
            this.báoCáoToolStripMenuItem,
            this.thoátToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(785, 29);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // danhMụcToolStripMenuItem
            // 
            this.danhMụcToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.thuêMáyToolStripMenuItem,
            this.bảoTrìToolStripMenuItem,
            this.tínhTiềnToolStripMenuItem});
            this.danhMụcToolStripMenuItem.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.danhMụcToolStripMenuItem.Name = "danhMụcToolStripMenuItem";
            this.danhMụcToolStripMenuItem.Size = new System.Drawing.Size(95, 25);
            this.danhMụcToolStripMenuItem.Text = "Danh mục";
            // 
            // thuêMáyToolStripMenuItem
            // 
            this.thuêMáyToolStripMenuItem.Name = "thuêMáyToolStripMenuItem";
            this.thuêMáyToolStripMenuItem.Size = new System.Drawing.Size(163, 26);
            this.thuêMáyToolStripMenuItem.Text = "Thuê máy";
            this.thuêMáyToolStripMenuItem.Click += new System.EventHandler(this.thuêMáyToolStripMenuItem_Click);
            // 
            // bảoTrìToolStripMenuItem
            // 
            this.bảoTrìToolStripMenuItem.Name = "bảoTrìToolStripMenuItem";
            this.bảoTrìToolStripMenuItem.Size = new System.Drawing.Size(163, 26);
            this.bảoTrìToolStripMenuItem.Text = "Bảo trì";
            this.bảoTrìToolStripMenuItem.Click += new System.EventHandler(this.bảoTrìToolStripMenuItem_Click);
            // 
            // tínhTiềnToolStripMenuItem
            // 
            this.tínhTiềnToolStripMenuItem.Name = "tínhTiềnToolStripMenuItem";
            this.tínhTiềnToolStripMenuItem.Size = new System.Drawing.Size(163, 26);
            this.tínhTiềnToolStripMenuItem.Text = "Tính tiền";
            this.tínhTiềnToolStripMenuItem.Click += new System.EventHandler(this.tínhTiềnToolStripMenuItem_Click);
            // 
            // tìmKiếmToolStripMenuItem
            // 
            this.tìmKiếmToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.máyToolStripMenuItem,
            this.nhânViênToolStripMenuItem});
            this.tìmKiếmToolStripMenuItem.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tìmKiếmToolStripMenuItem.Name = "tìmKiếmToolStripMenuItem";
            this.tìmKiếmToolStripMenuItem.Size = new System.Drawing.Size(89, 25);
            this.tìmKiếmToolStripMenuItem.Text = "Tìm kiếm";
            // 
            // máyToolStripMenuItem
            // 
            this.máyToolStripMenuItem.Name = "máyToolStripMenuItem";
            this.máyToolStripMenuItem.Size = new System.Drawing.Size(165, 26);
            this.máyToolStripMenuItem.Text = "Máy";
            this.máyToolStripMenuItem.Click += new System.EventHandler(this.máyToolStripMenuItem_Click);
            // 
            // nhânViênToolStripMenuItem
            // 
            this.nhânViênToolStripMenuItem.Name = "nhânViênToolStripMenuItem";
            this.nhânViênToolStripMenuItem.Size = new System.Drawing.Size(165, 26);
            this.nhânViênToolStripMenuItem.Text = "Nhân viên";
            this.nhânViênToolStripMenuItem.Click += new System.EventHandler(this.nhânViênToolStripMenuItem_Click);
            // 
            // dữLiệuToolStripMenuItem
            // 
            this.dữLiệuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.phòngToolStripMenuItem,
            this.nhânViênToolStripMenuItem1,
            this.máyTínhToolStripMenuItem,
            this.phầnCứngToolStripMenuItem,
            this.bảoTrìToolStripMenuItem1});
            this.dữLiệuToolStripMenuItem.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dữLiệuToolStripMenuItem.Name = "dữLiệuToolStripMenuItem";
            this.dữLiệuToolStripMenuItem.Size = new System.Drawing.Size(76, 25);
            this.dữLiệuToolStripMenuItem.Text = "Dữ liệu";
            // 
            // phòngToolStripMenuItem
            // 
            this.phòngToolStripMenuItem.Name = "phòngToolStripMenuItem";
            this.phòngToolStripMenuItem.Size = new System.Drawing.Size(168, 26);
            this.phòngToolStripMenuItem.Text = "Phòng";
            this.phòngToolStripMenuItem.Click += new System.EventHandler(this.phòngToolStripMenuItem_Click);
            // 
            // nhânViênToolStripMenuItem1
            // 
            this.nhânViênToolStripMenuItem1.Name = "nhânViênToolStripMenuItem1";
            this.nhânViênToolStripMenuItem1.Size = new System.Drawing.Size(168, 26);
            this.nhânViênToolStripMenuItem1.Text = "Nhân viên";
            this.nhânViênToolStripMenuItem1.Click += new System.EventHandler(this.nhânViênToolStripMenuItem1_Click);
            // 
            // máyTínhToolStripMenuItem
            // 
            this.máyTínhToolStripMenuItem.Name = "máyTínhToolStripMenuItem";
            this.máyTínhToolStripMenuItem.Size = new System.Drawing.Size(168, 26);
            this.máyTínhToolStripMenuItem.Text = "Máy tính";
            this.máyTínhToolStripMenuItem.Click += new System.EventHandler(this.máyTínhToolStripMenuItem_Click);
            // 
            // phầnCứngToolStripMenuItem
            // 
            this.phầnCứngToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.chipToolStripMenuItem,
            this.rAMToolStripMenuItem,
            this.ổCứngToolStripMenuItem,
            this.mànHìnhToolStripMenuItem,
            this.chuộtToolStripMenuItem,
            this.bànPhímToolStripMenuItem,
            this.loaToolStripMenuItem});
            this.phầnCứngToolStripMenuItem.Name = "phầnCứngToolStripMenuItem";
            this.phầnCứngToolStripMenuItem.Size = new System.Drawing.Size(168, 26);
            this.phầnCứngToolStripMenuItem.Text = "Phần cứng";
            // 
            // chipToolStripMenuItem
            // 
            this.chipToolStripMenuItem.Name = "chipToolStripMenuItem";
            this.chipToolStripMenuItem.Size = new System.Drawing.Size(161, 26);
            this.chipToolStripMenuItem.Text = "Chip";
            this.chipToolStripMenuItem.Click += new System.EventHandler(this.chipToolStripMenuItem_Click);
            // 
            // rAMToolStripMenuItem
            // 
            this.rAMToolStripMenuItem.Name = "rAMToolStripMenuItem";
            this.rAMToolStripMenuItem.Size = new System.Drawing.Size(161, 26);
            this.rAMToolStripMenuItem.Text = "RAM";
            this.rAMToolStripMenuItem.Click += new System.EventHandler(this.rAMToolStripMenuItem_Click);
            // 
            // ổCứngToolStripMenuItem
            // 
            this.ổCứngToolStripMenuItem.Name = "ổCứngToolStripMenuItem";
            this.ổCứngToolStripMenuItem.Size = new System.Drawing.Size(161, 26);
            this.ổCứngToolStripMenuItem.Text = "Ổ cứng";
            this.ổCứngToolStripMenuItem.Click += new System.EventHandler(this.ổCứngToolStripMenuItem_Click);
            // 
            // mànHìnhToolStripMenuItem
            // 
            this.mànHìnhToolStripMenuItem.Name = "mànHìnhToolStripMenuItem";
            this.mànHìnhToolStripMenuItem.Size = new System.Drawing.Size(161, 26);
            this.mànHìnhToolStripMenuItem.Text = "Màn hình";
            this.mànHìnhToolStripMenuItem.Click += new System.EventHandler(this.mànHìnhToolStripMenuItem_Click);
            // 
            // chuộtToolStripMenuItem
            // 
            this.chuộtToolStripMenuItem.Name = "chuộtToolStripMenuItem";
            this.chuộtToolStripMenuItem.Size = new System.Drawing.Size(161, 26);
            this.chuộtToolStripMenuItem.Text = "Chuột";
            this.chuộtToolStripMenuItem.Click += new System.EventHandler(this.chuộtToolStripMenuItem_Click);
            // 
            // bànPhímToolStripMenuItem
            // 
            this.bànPhímToolStripMenuItem.Name = "bànPhímToolStripMenuItem";
            this.bànPhímToolStripMenuItem.Size = new System.Drawing.Size(161, 26);
            this.bànPhímToolStripMenuItem.Text = "Bàn phím";
            this.bànPhímToolStripMenuItem.Click += new System.EventHandler(this.bànPhímToolStripMenuItem_Click);
            // 
            // loaToolStripMenuItem
            // 
            this.loaToolStripMenuItem.Name = "loaToolStripMenuItem";
            this.loaToolStripMenuItem.Size = new System.Drawing.Size(161, 26);
            this.loaToolStripMenuItem.Text = "Loa";
            this.loaToolStripMenuItem.Click += new System.EventHandler(this.loaToolStripMenuItem_Click);
            // 
            // bảoTrìToolStripMenuItem1
            // 
            this.bảoTrìToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nhàBảoTrìToolStripMenuItem,
            this.nguyênNhânToolStripMenuItem,
            this.giảiPhápToolStripMenuItem});
            this.bảoTrìToolStripMenuItem1.Name = "bảoTrìToolStripMenuItem1";
            this.bảoTrìToolStripMenuItem1.Size = new System.Drawing.Size(168, 26);
            this.bảoTrìToolStripMenuItem1.Text = "Bảo trì";
            // 
            // nhàBảoTrìToolStripMenuItem
            // 
            this.nhàBảoTrìToolStripMenuItem.Name = "nhàBảoTrìToolStripMenuItem";
            this.nhàBảoTrìToolStripMenuItem.Size = new System.Drawing.Size(187, 26);
            this.nhàBảoTrìToolStripMenuItem.Text = "Nhà bảo trì";
            this.nhàBảoTrìToolStripMenuItem.Click += new System.EventHandler(this.nhàBảoTrìToolStripMenuItem_Click);
            // 
            // nguyênNhânToolStripMenuItem
            // 
            this.nguyênNhânToolStripMenuItem.Name = "nguyênNhânToolStripMenuItem";
            this.nguyênNhânToolStripMenuItem.Size = new System.Drawing.Size(187, 26);
            this.nguyênNhânToolStripMenuItem.Text = "Nguyên nhân";
            this.nguyênNhânToolStripMenuItem.Click += new System.EventHandler(this.nguyênNhânToolStripMenuItem_Click);
            // 
            // giảiPhápToolStripMenuItem
            // 
            this.giảiPhápToolStripMenuItem.Name = "giảiPhápToolStripMenuItem";
            this.giảiPhápToolStripMenuItem.Size = new System.Drawing.Size(187, 26);
            this.giảiPhápToolStripMenuItem.Text = "Giải pháp";
            this.giảiPhápToolStripMenuItem.Click += new System.EventHandler(this.giảiPhápToolStripMenuItem_Click);
            // 
            // báoCáoToolStripMenuItem
            // 
            this.báoCáoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.phíBảoTrìToolStripMenuItem,
            this.tiềnThuêMáyToolStripMenuItem});
            this.báoCáoToolStripMenuItem.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.báoCáoToolStripMenuItem.Name = "báoCáoToolStripMenuItem";
            this.báoCáoToolStripMenuItem.Size = new System.Drawing.Size(79, 25);
            this.báoCáoToolStripMenuItem.Text = "Báo cáo";
            // 
            // phíBảoTrìToolStripMenuItem
            // 
            this.phíBảoTrìToolStripMenuItem.Name = "phíBảoTrìToolStripMenuItem";
            this.phíBảoTrìToolStripMenuItem.Size = new System.Drawing.Size(195, 26);
            this.phíBảoTrìToolStripMenuItem.Text = "Phí bảo trì";
            this.phíBảoTrìToolStripMenuItem.Click += new System.EventHandler(this.phíBảoTrìToolStripMenuItem_Click);
            // 
            // tiềnThuêMáyToolStripMenuItem
            // 
            this.tiềnThuêMáyToolStripMenuItem.Name = "tiềnThuêMáyToolStripMenuItem";
            this.tiềnThuêMáyToolStripMenuItem.Size = new System.Drawing.Size(195, 26);
            this.tiềnThuêMáyToolStripMenuItem.Text = "Tiền thuê máy";
            this.tiềnThuêMáyToolStripMenuItem.Click += new System.EventHandler(this.tiềnThuêMáyToolStripMenuItem_Click);
            // 
            // thoátToolStripMenuItem
            // 
            this.thoátToolStripMenuItem.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.thoátToolStripMenuItem.Name = "thoátToolStripMenuItem";
            this.thoátToolStripMenuItem.Size = new System.Drawing.Size(64, 25);
            this.thoátToolStripMenuItem.Text = "Thoát";
            this.thoátToolStripMenuItem.Click += new System.EventHandler(this.thoátToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(69, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(642, 59);
            this.label1.TabIndex = 1;
            this.label1.Text = "QUẢN LÝ CỬA HÀNG INTERNET";
            // 
            // picAnh
            // 
            this.picAnh.BackColor = System.Drawing.SystemColors.HighlightText;
            this.picAnh.Location = new System.Drawing.Point(0, 130);
            this.picAnh.Name = "picAnh";
            this.picAnh.Size = new System.Drawing.Size(785, 270);
            this.picAnh.TabIndex = 2;
            this.picAnh.TabStop = false;
            this.picAnh.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(352, 406);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 32);
            this.button1.TabIndex = 3;
            this.button1.Text = "Open";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.ClientSize = new System.Drawing.Size(785, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.picAnh);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMenu";
            this.Text = "QUẢN LÝ CỬA HÀNG INTERNET";
            this.Load += new System.EventHandler(this.frmMenu_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAnh)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem danhMụcToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thuêMáyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bảoTrìToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tínhTiềnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tìmKiếmToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem máyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nhânViênToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dữLiệuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem phòngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nhânViênToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem máyTínhToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem phầnCứngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chipToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rAMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ổCứngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mànHìnhToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chuộtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bànPhímToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bảoTrìToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem nhàBảoTrìToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nguyênNhânToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem giảiPhápToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem báoCáoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem phíBảoTrìToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tiềnThuêMáyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thoátToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loaToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox picAnh;
        private System.Windows.Forms.Button button1;
    }
}
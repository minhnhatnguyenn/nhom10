﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nhom10.Binh_s_Forms
{
    public partial class frmRam : Form
    {
        DataTable tblRam;
        public frmRam()
        {
            InitializeComponent();
        }

        private void frmRam_Load(object sender, EventArgs e)
        {
            //Functions.ketnoi();
            txtmaram.Enabled = false;
            btnluu.Enabled = false;
            btnboqua.Enabled = false;
            Load_DataGridView();
        }
        private void Load_DataGridView()
        {
            string sql;
            sql = "SELECT maram, tenram FROM tblRam";
            tblRam = Functions.Getdatatotable(sql);
            dgridram.DataSource = tblRam;
            dgridram.Columns[0].HeaderText = "Mã Ram";
            dgridram.Columns[1].HeaderText = "Tên Ram";
            dgridram.Columns[0].Width = 200;
            dgridram.Columns[1].Width = 300;
            // Không cho phép thêm mới dữ liệu trực tiếp trên lưới
            dgridram.AllowUserToAddRows = false;
            // Không cho phép sửa dữ liệu trực tiếp trên lưới
            dgridram.EditMode = DataGridViewEditMode.EditProgrammatically;
        }

        private void dgridram_Click(object sender, EventArgs e)
        {
            if (btnthem.Enabled == false)
            {
                MessageBox.Show("Đang ở chế độ thêm mới!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtmaram.Focus();
                return;
            }
            if (tblRam.Rows.Count == 0)
            {
                MessageBox.Show("Không có dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            txtmaram.Text = dgridram.CurrentRow.Cells["maram"].Value.ToString();
            txttenram.Text = dgridram.CurrentRow.Cells["tenram"].Value.ToString();
            btnsua.Enabled = true;
            btnxoa.Enabled = true;
            btnboqua.Enabled = true;
        }
        private void ResetValues()
        {
            txtmaram.Text = "";
            txttenram.Text = "";
        }

        private void btnthem_Click(object sender, EventArgs e)
        {
            btnsua.Enabled = false;
            btnxoa.Enabled = false;
            btnboqua.Enabled = true;
            btnluu.Enabled = true;
            btnthem.Enabled = false;
            ResetValues();
            txtmaram.Enabled = true;
            txtmaram.Focus();
        }

        private void btnluu_Click(object sender, EventArgs e)
        {
            string sql;
            if (txtmaram.Text == "")
            {
                MessageBox.Show("Bạn phải nhập mã ram ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtmaram.Focus();
                return;
            }
            if (txttenram.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên ram ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txttenram.Focus();
                return;
            }
            sql = "SELECT maram FROM tblRam WHERE maram=N'" + txtmaram.Text.Trim() + "'";
            if (Functions.CheckKey(sql))
            {
                MessageBox.Show("Mã ram này đã có, bạn phải nhập mã khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtmaram.Focus();
                txtmaram.Text = "";
                return;
            }
            sql = "INSERT INTO tblRam(maram,tenram) VALUES(N'" + txtmaram.Text + "',N'" + txttenram.Text + "')";
            Functions.RunSql(sql);
            Load_DataGridView();
            ResetValues();
            btnxoa.Enabled = true;
            btnthem.Enabled = true;
            btnsua.Enabled = true;
            btnboqua.Enabled = false;
            btnluu.Enabled = false;
            txtmaram.Enabled = false;
        }

        private void btnsua_Click(object sender, EventArgs e)
        {
            string sql;
            if (tblRam.Rows.Count == 0)
            {
                MessageBox.Show("Không còn dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtmaram.Text == "")
            {
                MessageBox.Show("Bạn chưa chọn bản ghi nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txttenram.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên nguyên nhân", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txttenram.Focus();
                return;
            }
            sql = "UPDATE tblRam SET tenram=N'" + txttenram.Text.ToString() + "' WHERE maram=N'" + txtmaram.Text + "'";
            Functions.RunSql(sql);
            Load_DataGridView();
            ResetValues();
            btnboqua.Enabled = false;
        }

        private void btnxoa_Click(object sender, EventArgs e)
        {
            string sql;
            if (tblRam.Rows.Count == 0)
            {
                MessageBox.Show("Không còn dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtmaram.Text == "")
            {
                MessageBox.Show("Bạn chưa chọn bản ghi nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (MessageBox.Show("Bạn có muốn xóa không?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                sql = "DELETE tblRam WHERE maram=N'" + txtmaram.Text + "'";
                Functions.RunSqlDel(sql);
                Load_DataGridView();
                ResetValues();
            }
        }

        private void btnboqua_Click(object sender, EventArgs e)
        {
            ResetValues();
            btnboqua.Enabled = false;
            btnthem.Enabled = true;
            btnxoa.Enabled = true;
            btnsua.Enabled = true;
            btnluu.Enabled = false;
            txtmaram.Enabled = false;
        }

        private void btnthoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nhom10
{
    public partial class frmChuot : Form
    {
        DataTable tblChuot;
        public frmChuot()
        {
            InitializeComponent();
        }

        private void frmChuot_Load(object sender, EventArgs e)
        {
            Functions.ketnoi();
            txtmachuot.Enabled = false;
            btnluu.Enabled = false;
            btnboqua.Enabled = false;
            Load_DataGridView();
        }
        private void Load_DataGridView()
        {
            string sql;
            sql = "SELECT machuot, tenchuot FROM tblChuot";
            tblChuot = Functions.Getdatatotable(sql);
            dgridchuot.DataSource = tblChuot;
            dgridchuot.Columns[0].HeaderText = "Mã chuột";
            dgridchuot.Columns[1].HeaderText = "Tên chuột";
            // Không cho phép thêm mới dữ liệu trực tiếp trên lưới
            dgridchuot.AllowUserToAddRows = false;
            // Không cho phép sửa dữ liệu trực tiếp trên lưới
            dgridchuot.EditMode = DataGridViewEditMode.EditProgrammatically;
        }

        private void dgridchuot_Click(object sender, EventArgs e)
        {
            if (btnthem.Enabled == false)
            {
                MessageBox.Show("Đang ở chế độ thêm mới!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtmachuot.Focus();
                return;
            }
            if (tblChuot.Rows.Count == 0)
            {
                MessageBox.Show("Không có dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            txtmachuot.Text = dgridchuot.CurrentRow.Cells["machuot"].Value.ToString();
            txttenchuot.Text = dgridchuot.CurrentRow.Cells["tenchuot"].Value.ToString();
            btnsua.Enabled = true;
            btnxoa.Enabled = true;
            btnboqua.Enabled = true;
        }
        private void ResetValues()
        {
            txtmachuot.Text = "";
            txttenchuot.Text = "";
        }

        private void btnthem_Click(object sender, EventArgs e)
        {
            btnsua.Enabled = false;
            btnxoa.Enabled = false;
            btnboqua.Enabled = true;
            btnluu.Enabled = true;
            btnthem.Enabled = false;
            ResetValues();
            txtmachuot.Enabled = true;
            txtmachuot.Focus();
        }

        private void btnluu_Click(object sender, EventArgs e)
        {
            string sql;
            if (txtmachuot.Text == "")
            {
                MessageBox.Show("Bạn phải nhập mã chuột ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtmachuot.Focus();
                return;
            }
            if (txttenchuot.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên chuột", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txttenchuot.Focus();
                return;
            }
            sql = "SELECT machuot FROM tblChuot WHERE machuot=N'" + txtmachuot.Text.Trim() + "'";
            if (Functions.CheckKey(sql))
            {
                MessageBox.Show("Mã bàn phím này đã có, bạn phải nhập mã khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtmachuot.Focus();
                txtmachuot.Text = "";
                return;
            }
            sql = "INSERT INTO tblChuot(machuot,tenchuot) VALUES(N'" + txtmachuot.Text + "',N'" + txttenchuot.Text + "')";
            Functions.RunSql(sql);
            Load_DataGridView();
            ResetValues();
            btnxoa.Enabled = true;
            btnthem.Enabled = true;
            btnsua.Enabled = true;
            btnboqua.Enabled = false;
            btnluu.Enabled = false;
            txtmachuot.Enabled = false;
        }

        private void btnsua_Click(object sender, EventArgs e)
        {
            string sql;
            if (tblChuot.Rows.Count == 0)
            {
                MessageBox.Show("Không còn dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtmachuot.Text == "")
            {
                MessageBox.Show("Bạn chưa chọn bản ghi nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txttenchuot.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên chuột", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txttenchuot.Focus();
                return;
            }
            sql = "UPDATE tblChuot SET tenchuot=N'" + txttenchuot.Text.ToString() + "' WHERE machuot=N'" + txtmachuot.Text + "'";
            Functions.RunSql(sql);
            Load_DataGridView();
            ResetValues();
            btnboqua.Enabled = false;
        }

        private void btnxoa_Click(object sender, EventArgs e)
        {
            string sql;
            if (tblChuot.Rows.Count == 0)
            {
                MessageBox.Show("Không còn dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtmachuot.Text == "")
            {
                MessageBox.Show("Bạn chưa chọn bản ghi nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (MessageBox.Show("Bạn có muốn xóa không?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                sql = "DELETE tblChuot WHERE machuot=N'" + txtmachuot.Text + "'";
                Functions.RunSqlDel(sql);
                Load_DataGridView();
                ResetValues();
            }
        }

        private void btnboqua_Click(object sender, EventArgs e)
        {
            ResetValues();
            btnboqua.Enabled = false;
            btnthem.Enabled = true;
            btnxoa.Enabled = true;
            btnsua.Enabled = true;
            btnluu.Enabled = false;
            txtmachuot.Enabled = false;
        }

        private void btnthoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

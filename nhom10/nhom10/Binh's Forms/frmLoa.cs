﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nhom10.Binh_s_Forms
{
    public partial class frmLoa : Form
    {
        DataTable tblLoa;
        public frmLoa()
        {
            InitializeComponent();
        }

        private void frmLoa_Load(object sender, EventArgs e)
        {
            //Functions.ketnoi();
            txtmaloa.Enabled = false;
            btnluu.Enabled = false;
            btnboqua.Enabled = false;
            Load_DataGridView();
        }
        private void Load_DataGridView()
        {
            string sql;
            sql = "SELECT maloa, tenloa FROM tblLoa";
            tblLoa = Functions.Getdatatotable(sql);
            dgridloa.DataSource = tblLoa;
            dgridloa.Columns[0].HeaderText = "Mã loa";
            dgridloa.Columns[1].HeaderText = "Tên loa";
            dgridloa.Columns[0].Width = 200;
            dgridloa.Columns[1].Width = 300;
            // Không cho phép thêm mới dữ liệu trực tiếp trên lưới
            dgridloa.AllowUserToAddRows = false;
            // Không cho phép sửa dữ liệu trực tiếp trên lưới
            dgridloa.EditMode = DataGridViewEditMode.EditProgrammatically;
        }

        private void dgridloa_Click(object sender, EventArgs e)
        {
            if (btnthem.Enabled == false)
            {
                MessageBox.Show("Đang ở chế độ thêm mới!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtmaloa.Focus();
                return;
            }
            if (tblLoa.Rows.Count == 0)
            {
                MessageBox.Show("Không có dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            txtmaloa.Text = dgridloa.CurrentRow.Cells["maloa"].Value.ToString();
            txttenloa.Text = dgridloa.CurrentRow.Cells["tenloa"].Value.ToString();
            btnsua.Enabled = true;
            btnxoa.Enabled = true;
            btnboqua.Enabled = true;
        }
        private void ResetValues()
        {
            txtmaloa.Text = "";
            txttenloa.Text = "";
        }

        private void btnthem_Click(object sender, EventArgs e)
        {
            btnsua.Enabled = false;
            btnxoa.Enabled = false;
            btnboqua.Enabled = true;
            btnluu.Enabled = true;
            btnthem.Enabled = false;
            ResetValues();
            txtmaloa.Enabled = true;
            txtmaloa.Focus();
        }

        private void btnluu_Click(object sender, EventArgs e)
        {
            string sql;
            if (txtmaloa.Text == "")
            {
                MessageBox.Show("Bạn phải nhập mã loa", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtmaloa.Focus();
                return;
            }
            if (txttenloa.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên loa", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtmaloa.Focus();
                return;
            }
            sql = "SELECT maloa FROM tblLoa WHERE maloa=N'" + txtmaloa.Text.Trim() + "'";
            if (Functions.CheckKey(sql))
            {
                MessageBox.Show("Mã loa này đã có, bạn phải nhập mã khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtmaloa.Focus();
                txttenloa.Text = "";
                return;
            }
            sql = "INSERT INTO tblLoa(maloa,tenloa) VALUES(N'" + txtmaloa.Text + "',N'" + txttenloa.Text + "')";
            Functions.RunSql(sql);
            Load_DataGridView();
            ResetValues();
            btnxoa.Enabled = true;
            btnthem.Enabled = true;
            btnsua.Enabled = true;
            btnboqua.Enabled = false;
            btnluu.Enabled = false;
            txtmaloa.Enabled = false;
        }

        private void btnsua_Click(object sender, EventArgs e)
        {
            string sql;
            if (tblLoa.Rows.Count == 0)
            {
                MessageBox.Show("Không còn dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtmaloa.Text == "")
            {
                MessageBox.Show("Bạn chưa chọn bản ghi nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txttenloa.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên loa", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txttenloa.Focus();
                return;
            }
            sql = "UPDATE tblLoa SET tenloa=N'" + txttenloa.Text.ToString() + "' WHERE maloa=N'" + txtmaloa.Text + "'";
            Functions.RunSql(sql);
            Load_DataGridView();
            ResetValues();
            btnboqua.Enabled = false;
        }

        private void btnxoa_Click(object sender, EventArgs e)
        {
            string sql;
            if (tblLoa.Rows.Count == 0)
            {
                MessageBox.Show("Không còn dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtmaloa.Text == "")
            {
                MessageBox.Show("Bạn chưa chọn bản ghi nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (MessageBox.Show("Bạn có muốn xóa không?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                sql = "DELETE tblLoa WHERE maloa=N'" + txtmaloa.Text + "'";
                Functions.RunSqlDel(sql);
                Load_DataGridView();
                ResetValues();
            }
        }

        private void btnboqua_Click(object sender, EventArgs e)
        {
            ResetValues();
            btnboqua.Enabled = false;
            btnthem.Enabled = true;
            btnxoa.Enabled = true;
            btnsua.Enabled = true;
            btnluu.Enabled = false;
            txtmaloa.Enabled = false;
        }

        private void btnthoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

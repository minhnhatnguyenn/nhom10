﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nhom10.Binh_s_Forms
{
    public partial class frmOcung : Form
    {
        DataTable tblOcung;
        public frmOcung()
        {
            InitializeComponent();
        }

        private void frmOcung_Load(object sender, EventArgs e)
        {
            //Functions.ketnoi();
            txtmaocung.Enabled = false;
            btnluu.Enabled = false;
            btnboqua.Enabled = false;
            Load_DataGridView();
        }
        private void Load_DataGridView()
        {
            string sql;
            sql = "SELECT maocung, tenocung , dungluong FROM tblOcung";
            tblOcung = Functions.Getdatatotable(sql);
            dgridocung.DataSource = tblOcung;
            dgridocung.Columns[0].HeaderText = "Mã ổ cứng";
            dgridocung.Columns[1].HeaderText = "Tên ổ cứng";
            dgridocung.Columns[2].HeaderText = "Dung lượng (GB)";
            dgridocung.Columns[0].Width = 200;
            dgridocung.Columns[1].Width = 200;
            dgridocung.Columns[2].Width = 200;
            // Không cho phép thêm mới dữ liệu trực tiếp trên lưới
            dgridocung.AllowUserToAddRows = false;
            // Không cho phép sửa dữ liệu trực tiếp trên lưới
            dgridocung.EditMode = DataGridViewEditMode.EditProgrammatically;
        }

        private void dgridocung_Click(object sender, EventArgs e)
        {
            if (btnthem.Enabled == false)
            {
                MessageBox.Show("Đang ở chế độ thêm mới!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtmaocung.Focus();
                return;
            }
            if (tblOcung.Rows.Count == 0)
            {
                MessageBox.Show("Không có dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            txtmaocung.Text = dgridocung.CurrentRow.Cells["maocung"].Value.ToString();
            txttenocung.Text = dgridocung.CurrentRow.Cells["tenocung"].Value.ToString();
            txtdungluong.Text = dgridocung.CurrentRow.Cells["dungluong"].Value.ToString();
            btnsua.Enabled = true;
            btnxoa.Enabled = true;
            btnboqua.Enabled = true;
        }
        private void ResetValues()
        {
            txtmaocung.Text = "";
            txttenocung.Text = "";
            txtdungluong.Text = "";
        }

        private void btnluu_Click(object sender, EventArgs e)
        {
            string sql;
            if (txtmaocung.Text == "")
            {
                MessageBox.Show("Bạn phải nhập mã ổ cứng ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtmaocung.Focus();
                return;
            }
            if (txttenocung.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên ổ cứng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txttenocung.Focus();
                return;
            }
            if (txtdungluong.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập dung lượng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtdungluong.Focus();
                return;
            }
            sql = "SELECT maocung FROM tblOcung WHERE maocung=N'" + txtmaocung.Text.Trim() + "'";
            if (Functions.CheckKey(sql))
            {
                MessageBox.Show("Mã ổ cứng này đã có, bạn phải nhập mã khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtmaocung.Focus();
                txtmaocung.Text = "";
                return;
            }
            sql = "INSERT INTO tblOcung(maocung,tenocung,dungluong) VALUES(N'" + txtmaocung.Text + "',N'" + txttenocung.Text + "',N'" + txtdungluong.Text + "')";
            Functions.RunSql(sql);
            Load_DataGridView();
            ResetValues();
            btnxoa.Enabled = true;
            btnthem.Enabled = true;
            btnsua.Enabled = true;
            btnboqua.Enabled = false;
            btnluu.Enabled = false;
            txtmaocung.Enabled = false;
        }

        private void btnthem_Click(object sender, EventArgs e)
        {
            btnsua.Enabled = false;
            btnxoa.Enabled = false;
            btnboqua.Enabled = true;
            btnluu.Enabled = true;
            btnthem.Enabled = false;
            ResetValues();
            txtmaocung.Enabled = true;
            txtmaocung.Focus();
        }

        private void btnsua_Click(object sender, EventArgs e)
        {
            string sql;
            if (tblOcung.Rows.Count == 0)
            {
                MessageBox.Show("Không còn dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtmaocung.Text == "")
            {
                MessageBox.Show("Bạn chưa chọn bản ghi nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txttenocung.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên ổ cứng ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txttenocung.Focus();
                return;
            }
            if (txtdungluong.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập dung lượng ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtdungluong.Focus();
                return;
            }
            sql = "UPDATE tblOcung SET  tenocung=N'" + txttenocung.Text.Trim().ToString() + "',dungluong=N'" + txtdungluong.Text.Trim().ToString() + "' WHERE maocung=N'" + txtmaocung.Text + "'";
            Functions.RunSql(sql);
            Load_DataGridView();
            ResetValues();
            btnboqua.Enabled = false;
        }

        private void btnxoa_Click(object sender, EventArgs e)
        {
            string sql;
            if (tblOcung.Rows.Count == 0)
            {
                MessageBox.Show("Không còn dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtmaocung.Text == "")
            {
                MessageBox.Show("Bạn chưa chọn bản ghi nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (MessageBox.Show("Bạn có muốn xóa không?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                sql = "DELETE tblOcung WHERE maocung=N'" + txtmaocung.Text + "'";
                Functions.RunSqlDel(sql);
                Load_DataGridView();
                ResetValues();
            }
        }

        private void btnboqua_Click(object sender, EventArgs e)
        {
            ResetValues();
            btnboqua.Enabled = false;
            btnthem.Enabled = true;
            btnxoa.Enabled = true;
            btnsua.Enabled = true;
            btnluu.Enabled = false;
            txtmaocung.Enabled = false;
        }

        private void btnthoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

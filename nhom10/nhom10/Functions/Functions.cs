﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace nhom10
{
    class Functions
    {
        public static SqlConnection Conn;
        public static string Connstring;
        public static void ketnoi()
        {
            Connstring = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=D:\\CSLT2\\nhom10\\nhom10\\nhom10\\database\\cuahanginternet.mdf;Integrated Security=True;Connect Timeout=30";
            Conn = new SqlConnection();
            Conn.ConnectionString = Connstring;
            Conn.Open();
        }
        public static void ngatketnoi()
        {
            if (Conn.State == ConnectionState.Open)
            {
                Conn.Close();
                Conn.Dispose();
                Conn = null;
            }
        }
        public static DataTable Getdatatotable(string sql)
        {
            SqlDataAdapter a = new SqlDataAdapter(sql, Functions.Conn);
            a.SelectCommand = new SqlCommand();
            a.SelectCommand.Connection = Functions.Conn;
            a.SelectCommand.CommandText = sql;
            DataTable bang = new DataTable();
            a.Fill(bang);
            return bang;
        }
        public static bool CheckKey(string sql)
        {
            SqlDataAdapter a = new SqlDataAdapter(sql, Functions.Conn);
            a.SelectCommand = new SqlCommand();
            a.SelectCommand.Connection = Functions.Conn;
            a.SelectCommand.CommandText = sql;
            DataTable bang = new DataTable();
            a.Fill(bang);
            if (bang.Rows.Count > 0)
                return true;
            else
                return false;
        }
        public static void RunSql(string sql)
        {
            SqlCommand a;
            a = new SqlCommand();
            a.Connection = Functions.Conn;
            a.CommandText = sql;
            try
            {
                a.ExecuteNonQuery();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            a.Dispose();
            a = null;
        }
        public static void RunSqlDel(string sql)
        {
            SqlCommand a = new SqlCommand();
            a.Connection = Functions.Conn;
            a.CommandText = sql;
            try
            {
                a.ExecuteNonQuery();
            }
            catch (System.Exception)
            {
                MessageBox.Show("Dữ liệu đang được dùng, không thể xóa!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            a.Dispose();
            a = null;
        }
        public static void Fillcombo(string sql, ComboBox cbobox, string ma, string ten)
        {
            SqlDataAdapter a = new SqlDataAdapter(sql, Functions.Conn);
            a.SelectCommand = new SqlCommand();
            a.SelectCommand.Connection = Functions.Conn;
            a.SelectCommand.CommandText = sql;
            DataTable bang = new DataTable();
            a.Fill(bang);
            cbobox.DataSource = bang;
            cbobox.ValueMember = ma;
            cbobox.DisplayMember = ten;
        }
        public static string GetFieldValues(string sql)
        {
            string ma = "";
            SqlCommand a = new SqlCommand(sql, Functions.Conn);
            SqlDataReader reader;
            reader = a.ExecuteReader();
            while (reader.Read())
            {
                ma = reader.GetValue(0).ToString();
            }
            reader.Close();
            return ma;
        }
        public static string ConvertDateTime(string d)
        {
            string[] parts = d.Split('/');
            string dt = String.Format("{0}/{1}/{2}", parts[1], parts[0], parts[2]);
            return dt;
        }
        public static bool IsDate(string d)
        {
            string[] parts = d.Split('/');
            if ((Convert.ToInt32(parts[0]) >= 1) && (Convert.ToInt32(parts[0]) <= 31) && (Convert.ToInt32(parts[1]) >= 1) && (Convert.ToInt32(parts[1]) <= 12) && (Convert.ToInt32(parts[2]) >= 1900))
                return true;
            else
                return false;
        }
        public static string ConvertTimeTo24(string hour)
        {
            string h = "";
            switch (hour)
            {
                case "1":
                    h = "13";
                    break;
                case "2":
                    h = "14";
                    break;
                case "3":
                    h = "15";
                    break;
                case "4":
                    h = "16";
                    break;
                case "5":
                    h = "17";
                    break;
                case "6":
                    h = "18";
                    break;
                case "7":
                    h = "19";
                    break;
                case "8":
                    h = "20";
                    break;
                case "9":
                    h = "21";
                    break;
                case "10":
                    h = "22";
                    break;
                case "11":
                    h = "23";
                    break;
                case "12":
                    h = "0";
                    break;
            }
            return h;
        }

    }
}

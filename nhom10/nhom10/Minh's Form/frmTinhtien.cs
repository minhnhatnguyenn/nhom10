﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;

namespace nhom10.Minh_s_Form
{
    public partial class frmTinhtien : Form
    {
        DataTable tbltinhtien;
        public frmTinhtien()
        {
            InitializeComponent();
        }
        private void ResetValues()
        {
            txtMathue.Text = "--Mã thuê";
            txtMamay.Text = "--Mã máy";
        }
        private void Load_dtgv()
        {
            string sql = "SELECT a.MaSTT,a.Mamay,a.Maphong,a.TenKhach,a.Giovao,a.Giora,a.Manv FROM tblthuemay a join tblmaytinh b ON a.Mamay = b.Mamay WHERE b.Tinhtrang = 'Not available'";
            tbltinhtien = Functions.Getdatatotable(sql);
            dtgv.DataSource = tbltinhtien;
            dtgv.Columns[0].HeaderText = "Mã Thuê";
            dtgv.Columns[1].HeaderText = "Máy";
            dtgv.Columns[2].HeaderText = "Phòng";
            dtgv.Columns[3].HeaderText = "Tên khách";
            dtgv.Columns[4].HeaderText = "Giờ vào";
            dtgv.Columns[5].HeaderText = "Giờ ra";
            dtgv.Columns[6].HeaderText = "Nhân viên";
            dtgv.AllowUserToAddRows = false;
            dtgv.EditMode = DataGridViewEditMode.EditProgrammatically;
        }
        private void frmTinhtien_Load(object sender, EventArgs e)
        {
            btnThanhtoan.Enabled = false; ;
            txtMathue.Enabled = false;
            txtMamay.Enabled = false;
            Load_dtgv();
            ResetValues();
        }

        private void btnThanhtoan_Click(object sender, EventArgs e)
        {
            string sql = "UPDATE tblthuemay SET Giora=CONVERT(CHAR(7),CURRENT_TIMESTAMP,108)";
            Functions.RunSql(sql);
            sql = "UPDATE tblmaytinh SET Tinhtrang='' WHERE Mamay='" + txtMamay.Text + "'";
            Functions.RunSql(sql);
            btnThanhtoan.Enabled = false;
        }

        private void dtgv_Click(object sender, EventArgs e)
        {
            if (dtgv.Rows.Count == 0)
            {
                MessageBox.Show("Không có máy đang thuê", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            txtMathue.Text = dtgv.CurrentRow.Cells["MaSTT"].Value.ToString();
            txtMamay.Text = dtgv.CurrentRow.Cells["Mamay"].Value.ToString();
            btnThanhtoan.Enabled = true;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

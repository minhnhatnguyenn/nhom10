﻿
namespace nhom10.Minh_s_Form
{
    partial class frmBaotri
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAdd = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtThoigian = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtChiphi = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDay_start = new System.Windows.Forms.TextBox();
            this.cboGiaiphap = new System.Windows.Forms.ComboBox();
            this.cboNguyennhan = new System.Windows.Forms.ComboBox();
            this.cboNBT = new System.Windows.Forms.ComboBox();
            this.cboMay = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnKetthuc = new System.Windows.Forms.Button();
            this.dtgv = new System.Windows.Forms.DataGridView();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgv)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Location = new System.Drawing.Point(523, 410);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 32);
            this.btnAdd.TabIndex = 21;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(621, 312);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 22);
            this.label3.TabIndex = 20;
            this.label3.Text = "Thời gian";
            // 
            // txtThoigian
            // 
            this.txtThoigian.Location = new System.Drawing.Point(723, 310);
            this.txtThoigian.Name = "txtThoigian";
            this.txtThoigian.Size = new System.Drawing.Size(121, 22);
            this.txtThoigian.TabIndex = 19;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(635, 352);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 22);
            this.label2.TabIndex = 18;
            this.label2.Text = "Chi phí";
            // 
            // txtChiphi
            // 
            this.txtChiphi.Location = new System.Drawing.Point(723, 350);
            this.txtChiphi.Name = "txtChiphi";
            this.txtChiphi.Size = new System.Drawing.Size(121, 22);
            this.txtChiphi.TabIndex = 17;
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(808, 410);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 32);
            this.btnCancel.TabIndex = 16;
            this.btnCancel.Text = "Đóng";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefresh.Location = new System.Drawing.Point(714, 410);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 32);
            this.btnRefresh.TabIndex = 15;
            this.btnRefresh.Text = "Bỏ qua";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(618, 410);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 32);
            this.btnSave.TabIndex = 14;
            this.btnSave.Text = "Lưu";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txtDay_start);
            this.groupBox2.Controls.Add(this.cboGiaiphap);
            this.groupBox2.Controls.Add(this.cboNguyennhan);
            this.groupBox2.Controls.Add(this.cboNBT);
            this.groupBox2.Controls.Add(this.cboMay);
            this.groupBox2.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox2.Location = new System.Drawing.Point(523, 67);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(360, 215);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "THÔNG TIN";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(73, 164);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 22);
            this.label1.TabIndex = 6;
            this.label1.Text = "Ngày bắt đầu";
            // 
            // txtDay_start
            // 
            this.txtDay_start.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDay_start.Location = new System.Drawing.Point(200, 162);
            this.txtDay_start.Name = "txtDay_start";
            this.txtDay_start.Size = new System.Drawing.Size(121, 26);
            this.txtDay_start.TabIndex = 5;
//            this.txtDay_start.TextChanged += new System.EventHandler(this.txtDay_start_TextChanged);
            // 
            // cboGiaiphap
            // 
            this.cboGiaiphap.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboGiaiphap.FormattingEnabled = true;
            this.cboGiaiphap.Location = new System.Drawing.Point(200, 103);
            this.cboGiaiphap.Name = "cboGiaiphap";
            this.cboGiaiphap.Size = new System.Drawing.Size(121, 29);
            this.cboGiaiphap.TabIndex = 3;
            this.cboGiaiphap.Text = "--Giải pháp";
            this.cboGiaiphap.SelectedIndexChanged += new System.EventHandler(this.cboGiaiphap_SelectedIndexChanged);
            // 
            // cboNguyennhan
            // 
            this.cboNguyennhan.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNguyennhan.FormattingEnabled = true;
            this.cboNguyennhan.Location = new System.Drawing.Point(40, 103);
            this.cboNguyennhan.Name = "cboNguyennhan";
            this.cboNguyennhan.Size = new System.Drawing.Size(121, 29);
            this.cboNguyennhan.TabIndex = 2;
            this.cboNguyennhan.Text = "--Nguyên nhân";
            // 
            // cboNBT
            // 
            this.cboNBT.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNBT.FormattingEnabled = true;
            this.cboNBT.Location = new System.Drawing.Point(200, 38);
            this.cboNBT.Name = "cboNBT";
            this.cboNBT.Size = new System.Drawing.Size(121, 29);
            this.cboNBT.TabIndex = 1;
            this.cboNBT.Text = "--Nhà bảo trì";
            // 
            // cboMay
            // 
            this.cboMay.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMay.FormattingEnabled = true;
            this.cboMay.Location = new System.Drawing.Point(40, 38);
            this.cboMay.Name = "cboMay";
            this.cboMay.Size = new System.Drawing.Size(121, 29);
            this.cboMay.TabIndex = 0;
            this.cboMay.Text = "--Máy bảo trì";
//            this.cboMay.SelectedIndexChanged += new System.EventHandler(this.cboMay_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnKetthuc);
            this.groupBox1.Controls.Add(this.dtgv);
            this.groupBox1.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.LavenderBlush;
            this.groupBox1.Location = new System.Drawing.Point(11, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(506, 421);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "MÁY ĐANG BẢO TRÌ";
            // 
            // btnKetthuc
            // 
            this.btnKetthuc.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKetthuc.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnKetthuc.Location = new System.Drawing.Point(382, 382);
            this.btnKetthuc.Name = "btnKetthuc";
            this.btnKetthuc.Size = new System.Drawing.Size(108, 33);
            this.btnKetthuc.TabIndex = 12;
            this.btnKetthuc.Text = "Xóa";
            this.btnKetthuc.UseVisualStyleBackColor = true;
            this.btnKetthuc.Click += new System.EventHandler(this.btnKetthuc_Click);
            // 
            // dtgv
            // 
            this.dtgv.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dtgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgv.Location = new System.Drawing.Point(21, 39);
            this.dtgv.Name = "dtgv";
            this.dtgv.RowHeadersWidth = 51;
            this.dtgv.Size = new System.Drawing.Size(469, 337);
            this.dtgv.TabIndex = 0;
            this.dtgv.Click += new System.EventHandler(this.dtgv_Click);
            // 
            // frmBaotri
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.ClientSize = new System.Drawing.Size(895, 514);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtThoigian);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtChiphi);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmBaotri";
            this.Text = "Bảo trì";
            this.Load += new System.EventHandler(this.frmBaotri_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtThoigian;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtChiphi;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDay_start;
        private System.Windows.Forms.ComboBox cboGiaiphap;
        private System.Windows.Forms.ComboBox cboNguyennhan;
        private System.Windows.Forms.ComboBox cboNBT;
        private System.Windows.Forms.ComboBox cboMay;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnKetthuc;
        private System.Windows.Forms.DataGridView dtgv;
    }
}
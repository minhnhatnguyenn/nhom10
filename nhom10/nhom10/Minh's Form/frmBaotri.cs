﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;

namespace nhom10.Minh_s_Form
{
    public partial class frmBaotri : Form
    {
        DataTable tblbaotri;
        public frmBaotri()
        {
            InitializeComponent();
        }
        private void ResetValues()
        {
            cboMay.Text = "--Máy bảo trì";
            cboNBT.Text = "--Nhà bảo trì";
            cboNguyennhan.Text = "--Nguyên nhân";
            cboGiaiphap.Text = "--Giải pháp";
            txtDay_start.Text = DateTime.Today.ToString("yyyy'/'MM'/'dd");
            txtThoigian.Text = "";
            txtChiphi.Text = "";
        }
        private void Load_dtgv()
        {
            string timeht = DateTime.Today.ToString("yyyy/MM/dd");
            string sql = "select b.Mamay,a.Ngaybaotri,a.Ngayketthuc from tblbaotri a inner join tblctbt b on a.Mabaotri=b.Mabaotri inner join tblmaytinh c on b.Mamay=c.Mamay where convert(varchar(20),a.Ngayketthuc,111) >= convert(varchar(20),getdate(),111) and c.tinhtrang=N'Đang bảo trì'";
            tblbaotri = Functions.Getdatatotable(sql);
            dtgv.DataSource = tblbaotri;
            dtgv.Columns[0].HeaderText = "Mã máy";
            dtgv.Columns[1].HeaderText = "Ngày bắt đầu";
            dtgv.Columns[2].HeaderText = "Ngày kết thúc";
            dtgv.Columns[0].Width = 130;
            dtgv.Columns[1].Width = 130;
            dtgv.Columns[2].Width = 130;
            dtgv.AllowUserToOrderColumns = false;
            dtgv.EditMode = DataGridViewEditMode.EditProgrammatically;
        }

        private void frmBaotri_Load(object sender, EventArgs e)
        {
            Functions.ketnoi();
            btnSave.Enabled = false;
            btnKetthuc.Enabled = false;
            cboMay.Enabled = false;
            cboNBT.Enabled = false;
            cboNguyennhan.Enabled = false;
            cboGiaiphap.Enabled = false;
            txtDay_start.Enabled = false;
            txtThoigian.Enabled = false;
            txtChiphi.Enabled = false;
            Load_dtgv();
            Functions.Fillcombo("SELECT Mamay FROM tblmaytinh", cboMay, "Mamay", "Mamay");
            Functions.Fillcombo("SELECT Manhabaotri FROM tblnhabaoTri", cboNBT, "Manhabaotri", "Manhabaotri");
            Functions.Fillcombo("SELECT Mangnhan,Tenngnhan FROM tblnguyenNhan", cboNguyennhan, "Mangnhan", "Tenngnhan");
            Functions.Fillcombo("SELECT Magiaiphap,Tengiaiphap FROM tblgiaiPhap", cboGiaiphap, "Magiaiphap", "Tengiaiphap");
            ResetValues();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            ResetValues();
            btnSave.Enabled = false;
            btnAdd.Enabled = true;
            btnKetthuc.Enabled = false;
            cboMay.Enabled = false;
            cboNBT.Enabled = false;
            cboNguyennhan.Enabled = false;
            cboGiaiphap.Enabled = false;
            txtDay_start.Enabled = false;
            txtThoigian.Enabled = false;
            txtChiphi.Enabled = false;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            btnAdd.Enabled = false;
            cboMay.Enabled = true;
            cboNBT.Enabled = true;
            cboNguyennhan.Enabled = true;
            cboGiaiphap.Enabled = true;
            txtDay_start.Enabled = true;
            btnSave.Enabled = true;
        }

        private void cboGiaiphap_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sql = "SELECT Thoigian FROM tblgiaiphap WHERE Tengiaiphap=N'" + cboGiaiphap.Text + "'";
            string ngay = Functions.GetFieldValues(sql);
            if (ngay == "0")
            {
                txtThoigian.Text = "Trong ngày";
            }
            else
            {
                txtThoigian.Text = ngay + " ngày";
            }
            string sql2 = "SELECT Chiphi FROM tblgiaiphap WHERE Tengiaiphap=N'" + cboGiaiphap.Text + "'";
            txtChiphi.Text = Functions.GetFieldValues(sql2);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cboMay.Text == "")
            {
                MessageBox.Show("Hãy chọn máy!!!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (cboNBT.Text == "")
            {
                MessageBox.Show("Hãy chọn nhà bảo trì!!!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (cboNguyennhan.Text == "")
            {
                MessageBox.Show("Hãy chọn nguyên nhân!!!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (cboGiaiphap.Text == "")
            {
                MessageBox.Show("Hãy chọn giải pháp!!!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtDay_start.Text == "")
            {
                MessageBox.Show("Điền ngày bắt đầu!!!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            string mann = Functions.GetFieldValues("SELECT Mangnhan FROM tblnguyennhan WHERE Tenngnhan=N'" + cboNguyennhan.Text + "'");
            string magp = Functions.GetFieldValues("SELECT Magiaiphap FROM tblgiaiphap WHERE Tengiaiphap=N'" + cboGiaiphap.Text + "'");
            int thoigian = Convert.ToInt32(Functions.GetFieldValues("SELECT Thoigian FROM tblgiaiphap WHERE Magiaiphap='" + magp + "'"));
            DateTime daystart = Convert.ToDateTime(txtDay_start.Text);
            DateTime dayend = daystart.AddDays(thoigian);
            string strdaystart = daystart.ToString("yyyy'/'MM'/'dd");
            string strdayend = dayend.ToString("yyyy'/'MM'/'dd");
            string sql = "insert into tblbaotri(Manhabaotri,Ngaybaotri,Ngayketthuc,Tongchiphi) values('" + cboNBT.Text + "','" + strdaystart + "','" + strdayend + "'," + txtChiphi.Text + ")";
            Functions.RunSql(sql);
            sql = "INSERT into tblctbt(Mamay,Magiaiphap,Mangnhan) values('" + cboMay.Text + "','" + magp + "','" + mann + "')";
            Functions.RunSql(sql);
            Functions.RunSql("UPDATE tblMayTinh SET TinhTrang=N'Đang bảo trì' WHERE Mamay='" + cboMay.Text + "'");
            Load_dtgv();
            ResetValues();
            btnAdd.Enabled = true;
            btnSave.Enabled = false;
            cboMay.Enabled = false;
            cboNBT.Enabled = false;
            cboGiaiphap.Enabled = false;
            cboNguyennhan.Enabled = false;
            txtDay_start.Enabled = false;
        }

        private void btnKetthuc_Click(object sender, EventArgs e)
        {
            string sql = "UPDATE tblMayTinh set TinhTrang ='' WHERE Mamay = '" + cboMay.Text + "'";
            Functions.RunSql(sql);
            Load_dtgv();
            btnKetthuc.Enabled = false;
        }

        private void dtgv_Click(object sender, EventArgs e)
        {
            cboMay.Text = dtgv.CurrentRow.Cells["Mamay"].Value.ToString();
            btnKetthuc.Enabled = true;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace nhom10.Minh_s_Form
{
    public partial class frmThuemay : Form
    {
        DataTable tblthuemay;
        public frmThuemay()
        {
            InitializeComponent();
           
        }
        private void Load_dtgv1()
        {
            string sql;
            sql = "SELECT a.Mamay,a.Maocung,a.Machip,a.Maram,a.Mamanhinh,a.Maloa,a.Machuot,a.Mabanphim,b.Dongia,a.Tinhtrang FROM tblmaytinh a JOIN tblphong b ON a.Maphong=b.Maphong WHERE  a.Tinhtrang = 'Available'";
            tblthuemay = Functions.Getdatatotable(sql);
            dtgv1.DataSource = tblthuemay;
            dtgv1.Columns[0].HeaderText = "Mã máy";
            dtgv1.Columns[1].HeaderText = "Ổ cứng";
            dtgv1.Columns[2].HeaderText = "Chip";
            dtgv1.Columns[3].HeaderText = "Ram";
            dtgv1.Columns[4].HeaderText = "Màn hình";
            dtgv1.Columns[5].HeaderText = "Loa";
            dtgv1.Columns[6].HeaderText = "Chuột";
            dtgv1.Columns[7].HeaderText = "Bàn phím";
            dtgv1.Columns[8].HeaderText = "Đơn giá";
            dtgv1.Columns[9].HeaderText = "Tình trạng";
            dtgv1.AllowUserToAddRows = false;
            dtgv1.EditMode = DataGridViewEditMode.EditProgrammatically;
        }

        private void ResetValues()
        {
            txtMamay.Text = "";
            cboPhong.SelectedIndex = -1;
            cboPhong.Text = "--Phòng";
            dtgv1.DataSource = null;
        }

        private void frmThuemay_Load(object sender, EventArgs e)
        {
            txtMamay.Enabled = false;
            btnThue.Enabled = false;
            Functions.Fillcombo("SELECT Maphong FROM tblphong ", cboPhong, "Maphong", "Maphong");
            ResetValues();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            ResetValues();
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTim_Click(object sender, EventArgs e)
        {
            string sql;
            if (cboPhong.SelectedIndex == -1)
            {
                MessageBox.Show("Hãy chọn phòng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            sql = "SELECT a.Mamay,a.Maocung,a.Machip,a.Maram,a.Mamanhinh,a.Maloa,a.Machuot,a.Mabanphim,b.Dongia,a.Ghichu FROM tblmaytinh a JOIN tblphong b ON a.Maphong=b.Maphong WHERE a.Tinhtrang= '' AND a.Maphong='" + cboPhong.Text.Trim() + "'";
            tblthuemay = Functions.Getdatatotable(sql);
            Load_dtgv1();
        }

        private void btnThue_Click(object sender, EventArgs e)
        {
            if (txtMamay.Text.Trim().Length == 0)
            {
                MessageBox.Show("Chọn máy để thuê", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            string sql = "UPDATE tblmaytinh SET Tinhtrang= N'Not available' WHERE Mamay='" + txtMamay.Text + "'";
            string sql1 = "UPDATE tblphong SET Giovao=N'CONVERT(char(7),CURRENT_TIMESTAMP,108)'";
            Functions.RunSql(sql);
            Functions.RunSql(sql1);
            ResetValues();
            btnThue.Enabled = false;
        }

        private void dtgv1_Click(object sender, EventArgs e)
        {

            if (dtgv1.Rows.Count == 0)
            {
                MessageBox.Show("Hãy chọn phòng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            txtMamay.Text = dtgv1.CurrentRow.Cells["Mamay"].Value.ToString();
            btnThue.Enabled = true;
        }

       private void timer1_Tick(object sender, EventArgs e)
        {
         
        }
    }
}

﻿CREATE TABLE [dbo].[tblmaytinh]
(
	[Mamay] NVARCHAR(50) NOT NULL PRIMARY KEY, 
    [Tenmay] NVARCHAR(50) NOT NULL, 
    [Maphong] NVARCHAR(50) NOT NULL, 
    [Maocung] NVARCHAR(50) NOT NULL, 
    [Madungluong] NVARCHAR(50) NOT NULL, 
    [Machip] NVARCHAR(50) NOT NULL, 
    [Maram] NVARCHAR(50) NOT NULL, 
    [Matocdo] NVARCHAR(50) NULL, 
    [Mamanhinh] NVARCHAR(50) NOT NULL, 
    [MacoMH] NVARCHAR(50) NOT NULL, 
    [Machuot] NVARCHAR(50) NOT NULL, 
    [Mabanphim] NVARCHAR(50) NOT NULL, 
    [Maodia] NVARCHAR(50) NOT NULL, 
    [Maloa] NVARCHAR(50) NOT NULL, 
    [Tinhtrang] NVARCHAR(50) NOT NULL, 
    [Ghichu] NVARCHAR(50) NULL,
	FOREIGN KEY ([Maphong]) REFERENCES [dbo].[tblphong] ([Maphong]),
	FOREIGN KEY ([Maocung]) REFERENCES [dbo].[tblocung] ([Maocung]),
	FOREIGN KEY ([Madungluong]) REFERENCES [dbo].[tbldungluong] ([Madungluong]),
	FOREIGN KEY ([Machip]) REFERENCES [dbo].[tblchip] ([Machip]),
	FOREIGN KEY ([Maram]) REFERENCES [dbo].[tblram] ([Maram]),
	FOREIGN KEY ([Matocdo]) REFERENCES [dbo].[tbltocdo] ([Matocdo]),
	FOREIGN KEY ([Mamanhinh]) REFERENCES [dbo].[tblmanhinh] ([Mamanhinh]),
	FOREIGN KEY ([MacoMH]) REFERENCES [dbo].[tblmacomh] ([MacoMH]),
	FOREIGN KEY ([Machuot]) REFERENCES [dbo].[tblchuot] ([Machuot]),
	FOREIGN KEY ([Mabanphim]) REFERENCES [dbo].[tblbanphim] ([Mabanphim]),
	FOREIGN KEY ([Maodia]) REFERENCES [dbo].[tblmaodia] ([Maodia]),
	FOREIGN KEY ([Maloa]) REFERENCES [dbo].[tblloa] ([Maloa]),
	
)

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using COMExcel = Microsoft.Office.Interop.Excel;

namespace nhom10.Oanh_s_Forms
{
    public partial class frmBaocaotienthue : Form
    {
        public frmBaocaotienthue()
        {
            InitializeComponent();
        }

        private void btntimkiem_Click(object sender, EventArgs e)
        {
            string tt;
            if (txtnam.Text == "")
            {
                MessageBox.Show("Nhập năm đi!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            tt = "SELECT SUM(Tongtien) as 'Doanh Thu' FROM tblthuemay Where 1=1";
            if (txtnam.Text == "" && cbothang.Text == "" && radArea1.Checked == false && radArea2.Checked == false && radArea3.Checked == false)
            {
                MessageBox.Show("Hãy nhập một điều kiện tìm kiếm!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (cbothang.Text != "")
            {
                tt = tt + "AND MONTH(Ngaythue) =" + cbothang.Text;
            }
            if (txtnam.Text != "")
            {
                tt = tt + "AND YEAR(Ngaythue) =" + txtnam.Text;
            }
            if (radArea1.Checked == true)
            {
                tt = tt + "AND Maphong='Area 1'";
            }
            if (radArea2.Checked == true)
            {
                tt = tt + "And Maphong='Area 2'";
            }
            if (radArea3.Checked == true)
            {
                tt = tt + "And Maphong='Area 3'";
            }
            if (cboquy.Text == "1")
            {
                tt = tt + "AND MONTH(Ngaythue) BETWEEN '1' AND '3'";
            }
            if (cboquy.Text == "2")
            {
                tt = tt + "AND MONTH(Ngaythue) BETWEEN '4' AND '6'";
            }
            if (cboquy.Text == "3")
            {
                tt = tt + "AND MONTH(Ngaythue) BETWEEN '7' AND '9'";
            }
            if (cboquy.Text == "4")
            {
                tt = tt + "AND MONTH(Ngaythue) BETWEEN '10' AND '12'";
            }
            lbltongtien.Text = lbltongtien.Text + Functions.GetFieldValues(tt);
            btntimkiem.Enabled = false;
            cboquy.Enabled = false;
            radArea1.Enabled = false;
            radArea2.Enabled = false;
            radArea3.Enabled = false;
 
       }
        private void Reset_Values()
        {
            cbothang.SelectedIndex = -1;
            txtnam.Text = "";
            cboquy.SelectedIndex = -1;
            radArea1.Checked = false;
            radArea2.Checked = false;
            radArea3.Checked = false;
            cboquy.Enabled = true;
            cbothang.Enabled = true;
            lbltongtien.Text = "Tổng tiền ";
        }

        private void btnlammoi_Click(object sender, EventArgs e)
        {
            Reset_Values();
            btntimkiem.Enabled = true;
            radArea1.Enabled = true;
            radArea2.Enabled = true;
            radArea3.Enabled = true;
        }

        private void cbothang_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboquy.Enabled = false;
        }

        private void cboquy_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbothang.Enabled = false;
        }

        private void btnhuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnxuat_Click(object sender, EventArgs e)
        {
            // Khởi động chương trình Excel
            COMExcel.Application exApp = new COMExcel.Application();
            COMExcel.Workbook exBook; //Trong 1 chương trình Excel có nhiều Workbook
            COMExcel.Worksheet exSheet; //Trong 1 Workbook có nhiều Worksheet
            COMExcel.Range exRange;
            string tt;
            int hang = 0, cot = 0;
            DataTable ThongtinNBT, ThongtinBT;
            exBook = exApp.Workbooks.Add(COMExcel.XlWBATemplate.xlWBATWorksheet);
            exSheet = exBook.Worksheets[1];
            // Định dạng chung
            exRange = exSheet.Cells[1, 1];
            exRange.Range["A1:B3"].Font.Size = 10;
            exRange.Range["A1:B3"].Font.Name = "Calibri";
            exRange.Range["A1:B3"].Font.Bold = true;
            exRange.Range["A1:B3"].Font.ColorIndex = 5; //Màu xanh da trời
            exRange.Range["A1:A1"].ColumnWidth = 7;
            exRange.Range["B1:B1"].ColumnWidth = 15;
            exRange.Range["A1:B1"].MergeCells = true;
            exRange.Range["A1:B1"].HorizontalAlignment = COMExcel.XlHAlign.xlHAlignCenter;
            exRange.Range["A1:B1"].Value = "Quán Internet - CYBER10";
            exRange.Range["A2:B2"].MergeCells = true;
            exRange.Range["A2:B2"].HorizontalAlignment = COMExcel.XlHAlign.xlHAlignCenter;
            exRange.Range["A2:B2"].Value = "Banking Academy";
            exRange.Range["A3:B3"].MergeCells = true;
            exRange.Range["A3:B3"].HorizontalAlignment = COMExcel.XlHAlign.xlHAlignCenter;
            exRange.Range["A3:B3"].Value = "Contact: (08)68686868";
            exRange.Range["C2:E2"].Font.Size = 14;
            exRange.Range["C2:E2"].Font.Name = "Calibri";
            exRange.Range["C2:F2"].Font.Bold = true;
            exRange.Range["C2:F2"].Font.ColorIndex = 3; //Màu đỏ
            exRange.Range["C2:F2"].MergeCells = true;
            exRange.Range["C2:F2"].HorizontalAlignment = COMExcel.XlHAlign.xlHAlignCenter;
            exRange.Range["C2:F2"].Value = "BÁO CÁO TỔNG TIỀN";
            // Biểu diễn thông tin chung của Báo cáo chi phí
            tt = "SELECT Maphong FROM tblthuemay";
            ThongtinNBT = Functions.Getdatatotable(tt);
            exRange.Range["B6:C9"].Font.Size = 12;
            exRange.Range["B6:C9"].Font.Name = "Calibri";
            exRange.Range["B6:B6"].Value = "Mã Phòng:";
            //Lấy thông tin bảo trì
            if (txtnam.Text == "")
            {
                MessageBox.Show("Nhập năm đi!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            tt = "SELECT SUM(Tongtien) as 'Doanh Thu' FROM tblthuemay Where 1=1";
            if (txtnam.Text == "" && cbothang.Text == "" && radArea1.Checked == false && radArea2.Checked == false && radArea3.Checked == false)
            {
                MessageBox.Show("Hãy nhập một điều kiện tìm kiếm!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (cbothang.Text != "")
            {
                tt = tt + "AND MONTH(Ngaythue) =" + cbothang.Text;
            }
            if (txtnam.Text != "")
            {
                tt = tt + "AND YEAR(Ngaythue) =" + txtnam.Text;
            }
            if (radArea1.Checked == true)
            {
                tt = tt + "AND Maphong='Area 1'";
            }
            if (radArea2.Checked == true)
            {
                tt = tt + "And Maphong='Area 2'";
            }
            if (radArea3.Checked == true)
            {
                tt = tt + "And Maphong='Area 3'";
            }
            if (cboquy.Text == "1")
            {
                tt = tt + "AND MONTH(Ngaythue) BETWEEN '1' AND '3'";
            }
            if (cboquy.Text == "2")
            {
                tt = tt + "AND MONTH(Ngaythue) BETWEEN '4' AND '6'";
            }
            if (cboquy.Text == "3")
            {
                tt = tt + "AND MONTH(Ngaythue) BETWEEN '7' AND '9'";
            }
            if (cboquy.Text == "4")
            {
                tt = tt + "AND MONTH(Ngaythue) BETWEEN '10' AND '12'";
            }

            btntimkiem.Enabled = false;
            ThongtinBT = Functions.Getdatatotable(tt);

            //Tạo dòng tiêu đề bảng
            exRange.Range["B7:B7"].Font.Bold = true;
            exRange.Range["B7:B7"].HorizontalAlignment = COMExcel.XlHAlign.xlHAlignCenter;
            exRange.Range["B7:B7"].ColumnWidth = 15;
            exRange.Range["B7:B7"].Value = "Tổng tiền";
            if (radArea1.Checked == true && cboquy.Enabled == false)
            {

                exRange.Range["B4:B4"].Value = "Năm:";
                exRange.Range["C4:C4"].Value = txtnam.Text;
                exRange.Range["B5:B5"].Value = "Tháng:";
                exRange.Range["C5:C5"].Value = cbothang.Text;
                exRange.Range["C6:C6"].Value = "Area 1";
            }
            if (radArea2.Checked == true && cboquy.Enabled == false)
            {
                exRange.Range["B4:B4"].Value = "Năm:";
                exRange.Range["C4:C4"].Value = txtnam.Text;
                exRange.Range["B5:B5"].Value = "Tháng:";
                exRange.Range["C5:C5"].Value = cbothang.Text;
                exRange.Range["C6:C6"].Value = "Area 2";
            }
            if (radArea3.Checked == true && cboquy.Enabled == false)
            {
                exRange.Range["B4:B4"].Value = "Năm:";
                exRange.Range["C4:C4"].Value = txtnam.Text;
                exRange.Range["B5:B5"].Value = "Tháng:";
                exRange.Range["C5:C5"].Value = cbothang.Text;
                exRange.Range["C6:C6"].Value = "Area 3";
            }
            if (radArea3.Checked == false && radArea3.Checked == false && radArea3.Checked == false && cboquy.Enabled == false)
            {
                exRange.Range["B5:B5"].Value = "Tháng:";
                exRange.Range["C5:C5"].Value = cbothang.Text;
                exRange.Range["B6:B6"].Value = "Năm:";
                exRange.Range["C6:C6"].Value = txtnam.Text;

            }
            if (radArea1.Checked == true && cbothang.Enabled == false)
            {
                exRange.Range["B4:B4"].Value = "Năm:";
                exRange.Range["C4:C4"].Value = txtnam.Text;
                exRange.Range["B5:B5"].Value = "Quý:";
                exRange.Range["C5:C5"].Value = cboquy.Text;
                exRange.Range["C6:C6"].Value = "Area 1";
            }
            if (radArea2.Checked == true && cbothang.Enabled == false)
            {
                exRange.Range["B4:B4"].Value = "Năm:";
                exRange.Range["C4:C4"].Value = txtnam.Text;
                exRange.Range["B5:B5"].Value = "Quý:";
                exRange.Range["C5:C5"].Value = cboquy.Text;
                exRange.Range["C6:C6"].Value = "Area 2";
            }
            if (radArea3.Checked == true && cbothang.Enabled == false)
            {
                exRange.Range["B4:B4"].Value = "Năm:";
                exRange.Range["C4:C4"].Value = txtnam.Text;
                exRange.Range["B5:B5"].Value = "Quý:";
                exRange.Range["C5:C5"].Value = cboquy.Text;
                exRange.Range["C6:C6"].Value = "Area 3";
            }
            if (radArea1.Checked == false && radArea2.Checked == false && radArea3.Checked == false && cbothang.Enabled == false)
            {
                exRange.Range["B5:B5"].Value = "Quý";
                exRange.Range["C5:C5"].Value = cboquy.Text;
                exRange.Range["B6:B6"].Value = "Năm:";
                exRange.Range["C6:C6"].Value = txtnam.Text;

            }
            for (hang = 0; hang <= ThongtinBT.Rows.Count - 1; hang++)
            {
                for (cot = 0; cot <= ThongtinBT.Columns.Count - 1; cot++)
                    //Điền thông tin hàng từ cột thứ 1, dòng 12
                    exSheet.Cells[cot + 3][hang + 7] = ThongtinBT.Rows[hang][cot].ToString();
            }
            exSheet.Name = "BÁO CÁO TIỀN THUÊ";
            exApp.Visible = true;

        }

        private void frmBaocaotienthue_Load(object sender, EventArgs e)
        {

        }

        private void txtnam_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(((e.KeyChar >= '0') && (e.KeyChar <= '9')) || (Convert.ToInt32(e.KeyChar) == 8) || (Convert.ToInt32(e.KeyChar) == 13))
            {
                e.Handled = false;
            }    
            else
            {
                e.Handled = true;
            }    
        }
    }
}

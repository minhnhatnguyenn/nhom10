﻿
namespace nhom10.Oanh_s_Forms
{
    partial class frmBaocaotienthue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbothang = new System.Windows.Forms.ComboBox();
            this.cboquy = new System.Windows.Forms.ComboBox();
            this.radArea1 = new System.Windows.Forms.RadioButton();
            this.radArea2 = new System.Windows.Forms.RadioButton();
            this.radArea3 = new System.Windows.Forms.RadioButton();
            this.lbltongtien = new System.Windows.Forms.Label();
            this.btntimkiem = new System.Windows.Forms.Button();
            this.btnlammoi = new System.Windows.Forms.Button();
            this.btnhuy = new System.Windows.Forms.Button();
            this.btnxuat = new System.Windows.Forms.Button();
            this.txtnam = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(139, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(535, 46);
            this.label1.TabIndex = 0;
            this.label1.Text = "DANH MỤC BÁO CÁO TIỀN THUÊ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(30, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tháng ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(290, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 23);
            this.label3.TabIndex = 2;
            this.label3.Text = "Quý";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(541, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 23);
            this.label4.TabIndex = 3;
            this.label4.Text = "Năm";
            // 
            // cbothang
            // 
            this.cbothang.FormattingEnabled = true;
            this.cbothang.Location = new System.Drawing.Point(107, 75);
            this.cbothang.Name = "cbothang";
            this.cbothang.Size = new System.Drawing.Size(145, 24);
            this.cbothang.TabIndex = 4;
            this.cbothang.SelectedIndexChanged += new System.EventHandler(this.cbothang_SelectedIndexChanged);
            // 
            // cboquy
            // 
            this.cboquy.FormattingEnabled = true;
            this.cboquy.Location = new System.Drawing.Point(365, 75);
            this.cboquy.Name = "cboquy";
            this.cboquy.Size = new System.Drawing.Size(145, 24);
            this.cboquy.TabIndex = 5;
            this.cboquy.SelectedIndexChanged += new System.EventHandler(this.cboquy_SelectedIndexChanged);
            // 
            // radArea1
            // 
            this.radArea1.AutoSize = true;
            this.radArea1.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radArea1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radArea1.Location = new System.Drawing.Point(117, 142);
            this.radArea1.Name = "radArea1";
            this.radArea1.Size = new System.Drawing.Size(83, 27);
            this.radArea1.TabIndex = 7;
            this.radArea1.TabStop = true;
            this.radArea1.Text = "Area 1";
            this.radArea1.UseVisualStyleBackColor = true;
            // 
            // radArea2
            // 
            this.radArea2.AutoSize = true;
            this.radArea2.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radArea2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radArea2.Location = new System.Drawing.Point(365, 142);
            this.radArea2.Name = "radArea2";
            this.radArea2.Size = new System.Drawing.Size(83, 27);
            this.radArea2.TabIndex = 8;
            this.radArea2.TabStop = true;
            this.radArea2.Text = "Area 2";
            this.radArea2.UseVisualStyleBackColor = true;
            // 
            // radArea3
            // 
            this.radArea3.AutoSize = true;
            this.radArea3.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radArea3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radArea3.Location = new System.Drawing.Point(598, 142);
            this.radArea3.Name = "radArea3";
            this.radArea3.Size = new System.Drawing.Size(83, 27);
            this.radArea3.TabIndex = 9;
            this.radArea3.TabStop = true;
            this.radArea3.Text = "Area 3";
            this.radArea3.UseVisualStyleBackColor = true;
            // 
            // lbltongtien
            // 
            this.lbltongtien.AutoSize = true;
            this.lbltongtien.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltongtien.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbltongtien.Location = new System.Drawing.Point(211, 208);
            this.lbltongtien.Name = "lbltongtien";
            this.lbltongtien.Size = new System.Drawing.Size(104, 29);
            this.lbltongtien.TabIndex = 10;
            this.lbltongtien.Text = "Tổng tiền";
            // 
            // btntimkiem
            // 
            this.btntimkiem.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btntimkiem.Location = new System.Drawing.Point(94, 285);
            this.btntimkiem.Name = "btntimkiem";
            this.btntimkiem.Size = new System.Drawing.Size(103, 36);
            this.btntimkiem.TabIndex = 11;
            this.btntimkiem.Text = "Tìm kiếm";
            this.btntimkiem.UseVisualStyleBackColor = true;
            this.btntimkiem.Click += new System.EventHandler(this.btntimkiem_Click);
            // 
            // btnlammoi
            // 
            this.btnlammoi.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnlammoi.Location = new System.Drawing.Point(267, 285);
            this.btnlammoi.Name = "btnlammoi";
            this.btnlammoi.Size = new System.Drawing.Size(103, 36);
            this.btnlammoi.TabIndex = 12;
            this.btnlammoi.Text = "Làm mới";
            this.btnlammoi.UseVisualStyleBackColor = true;
            this.btnlammoi.Click += new System.EventHandler(this.btnlammoi_Click);
            // 
            // btnhuy
            // 
            this.btnhuy.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnhuy.Location = new System.Drawing.Point(436, 285);
            this.btnhuy.Name = "btnhuy";
            this.btnhuy.Size = new System.Drawing.Size(103, 36);
            this.btnhuy.TabIndex = 13;
            this.btnhuy.Text = "Hủy";
            this.btnhuy.UseVisualStyleBackColor = true;
            this.btnhuy.Click += new System.EventHandler(this.btnhuy_Click);
            // 
            // btnxuat
            // 
            this.btnxuat.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnxuat.Location = new System.Drawing.Point(598, 285);
            this.btnxuat.Name = "btnxuat";
            this.btnxuat.Size = new System.Drawing.Size(116, 36);
            this.btnxuat.TabIndex = 14;
            this.btnxuat.Text = "Xuất báo cáo";
            this.btnxuat.UseVisualStyleBackColor = true;
            this.btnxuat.Click += new System.EventHandler(this.btnxuat_Click);
            // 
            // txtnam
            // 
            this.txtnam.Location = new System.Drawing.Point(607, 75);
            this.txtnam.Name = "txtnam";
            this.txtnam.Size = new System.Drawing.Size(145, 22);
            this.txtnam.TabIndex = 15;
            this.txtnam.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtnam_KeyPress);
            // 
            // frmBaocaotienthue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.ClientSize = new System.Drawing.Size(800, 371);
            this.Controls.Add(this.txtnam);
            this.Controls.Add(this.btnxuat);
            this.Controls.Add(this.btnhuy);
            this.Controls.Add(this.btnlammoi);
            this.Controls.Add(this.btntimkiem);
            this.Controls.Add(this.lbltongtien);
            this.Controls.Add(this.radArea3);
            this.Controls.Add(this.radArea2);
            this.Controls.Add(this.radArea1);
            this.Controls.Add(this.cboquy);
            this.Controls.Add(this.cbothang);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmBaocaotienthue";
            this.Text = "frmBaocaotienthue";
            this.Load += new System.EventHandler(this.frmBaocaotienthue_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbothang;
        private System.Windows.Forms.ComboBox cboquy;
        private System.Windows.Forms.RadioButton radArea1;
        private System.Windows.Forms.RadioButton radArea2;
        private System.Windows.Forms.RadioButton radArea3;
        private System.Windows.Forms.Label lbltongtien;
        private System.Windows.Forms.Button btntimkiem;
        private System.Windows.Forms.Button btnlammoi;
        private System.Windows.Forms.Button btnhuy;
        private System.Windows.Forms.Button btnxuat;
        private System.Windows.Forms.TextBox txtnam;
    }
}
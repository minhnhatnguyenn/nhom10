﻿
namespace nhom10.Oanh_s_Forms
{
    partial class frmBaocaocpbt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblquy = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbothang = new System.Windows.Forms.ComboBox();
            this.cboquy = new System.Windows.Forms.ComboBox();
            this.txtnam = new System.Windows.Forms.TextBox();
            this.dgridcpbt = new System.Windows.Forms.DataGridView();
            this.btnbaocao = new System.Windows.Forms.Button();
            this.btnboqua = new System.Windows.Forms.Button();
            this.btninbaocao = new System.Windows.Forms.Button();
            this.btnthoat = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgridcpbt)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(178, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(445, 46);
            this.label1.TabIndex = 0;
            this.label1.Text = "DANH MỤC BÁO CÁO CPBT";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(41, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tháng";
            // 
            // lblquy
            // 
            this.lblquy.AutoSize = true;
            this.lblquy.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblquy.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblquy.Location = new System.Drawing.Point(288, 87);
            this.lblquy.Name = "lblquy";
            this.lblquy.Size = new System.Drawing.Size(42, 23);
            this.lblquy.TabIndex = 2;
            this.lblquy.Text = "Quý";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(523, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 23);
            this.label4.TabIndex = 3;
            this.label4.Text = "Năm";
            // 
            // cbothang
            // 
            this.cbothang.FormattingEnabled = true;
            this.cbothang.Location = new System.Drawing.Point(104, 87);
            this.cbothang.Name = "cbothang";
            this.cbothang.Size = new System.Drawing.Size(148, 24);
            this.cbothang.TabIndex = 4;
            this.cbothang.SelectedIndexChanged += new System.EventHandler(this.cbothang_SelectedIndexChanged);
            // 
            // cboquy
            // 
            this.cboquy.FormattingEnabled = true;
            this.cboquy.Location = new System.Drawing.Point(352, 87);
            this.cboquy.Name = "cboquy";
            this.cboquy.Size = new System.Drawing.Size(148, 24);
            this.cboquy.TabIndex = 5;
            this.cboquy.SelectedIndexChanged += new System.EventHandler(this.cboquy_SelectedIndexChanged);
            // 
            // txtnam
            // 
            this.txtnam.Location = new System.Drawing.Point(587, 87);
            this.txtnam.Name = "txtnam";
            this.txtnam.Size = new System.Drawing.Size(148, 22);
            this.txtnam.TabIndex = 6;
            this.txtnam.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtnam_KeyPress);
            // 
            // dgridcpbt
            // 
            this.dgridcpbt.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgridcpbt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgridcpbt.Location = new System.Drawing.Point(104, 165);
            this.dgridcpbt.Name = "dgridcpbt";
            this.dgridcpbt.RowHeadersWidth = 51;
            this.dgridcpbt.RowTemplate.Height = 24;
            this.dgridcpbt.Size = new System.Drawing.Size(601, 150);
            this.dgridcpbt.TabIndex = 7;
            // 
            // btnbaocao
            // 
            this.btnbaocao.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbaocao.Location = new System.Drawing.Point(104, 360);
            this.btnbaocao.Name = "btnbaocao";
            this.btnbaocao.Size = new System.Drawing.Size(75, 31);
            this.btnbaocao.TabIndex = 8;
            this.btnbaocao.Text = "Báo cáo";
            this.btnbaocao.UseVisualStyleBackColor = true;
            this.btnbaocao.Click += new System.EventHandler(this.btnbaocao_Click);
            // 
            // btnboqua
            // 
            this.btnboqua.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnboqua.Location = new System.Drawing.Point(277, 360);
            this.btnboqua.Name = "btnboqua";
            this.btnboqua.Size = new System.Drawing.Size(75, 31);
            this.btnboqua.TabIndex = 9;
            this.btnboqua.Text = "Bỏ qua";
            this.btnboqua.UseVisualStyleBackColor = true;
            this.btnboqua.Click += new System.EventHandler(this.btnboqua_Click);
            // 
            // btninbaocao
            // 
            this.btninbaocao.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btninbaocao.Location = new System.Drawing.Point(450, 360);
            this.btninbaocao.Name = "btninbaocao";
            this.btninbaocao.Size = new System.Drawing.Size(90, 31);
            this.btninbaocao.TabIndex = 10;
            this.btninbaocao.Text = "In báo cáo";
            this.btninbaocao.UseVisualStyleBackColor = true;
            this.btninbaocao.Click += new System.EventHandler(this.btninbaocao_Click);
            // 
            // btnthoat
            // 
            this.btnthoat.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnthoat.Location = new System.Drawing.Point(630, 360);
            this.btnthoat.Name = "btnthoat";
            this.btnthoat.Size = new System.Drawing.Size(75, 31);
            this.btnthoat.TabIndex = 11;
            this.btnthoat.Text = "Thoát";
            this.btnthoat.UseVisualStyleBackColor = true;
            this.btnthoat.Click += new System.EventHandler(this.btnthoat_Click);
            // 
            // frmBaocaocpbt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnthoat);
            this.Controls.Add(this.btninbaocao);
            this.Controls.Add(this.btnboqua);
            this.Controls.Add(this.btnbaocao);
            this.Controls.Add(this.dgridcpbt);
            this.Controls.Add(this.txtnam);
            this.Controls.Add(this.cboquy);
            this.Controls.Add(this.cbothang);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblquy);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmBaocaocpbt";
            this.Text = "frmBaocaocpbt";
            this.Load += new System.EventHandler(this.frmBaocaocpbt_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgridcpbt)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblquy;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbothang;
        private System.Windows.Forms.ComboBox cboquy;
        private System.Windows.Forms.TextBox txtnam;
        private System.Windows.Forms.DataGridView dgridcpbt;
        private System.Windows.Forms.Button btnbaocao;
        private System.Windows.Forms.Button btnboqua;
        private System.Windows.Forms.Button btninbaocao;
        private System.Windows.Forms.Button btnthoat;
    }
}
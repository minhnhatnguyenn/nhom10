﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using COMExcel = Microsoft.Office.Interop.Excel;

namespace nhom10.Oanh_s_Forms
{
    public partial class frmBaocaocpbt : Form
    {
        DataTable tblcpbt;
        public frmBaocaocpbt()
        {
            InitializeComponent();
        }

        private void frmBaocaocpbt_Load(object sender, EventArgs e)
        {
            btnboqua.Enabled = false;
            btninbaocao.Enabled = false;
            Reset_Values();
            Load_DataGridView();
        }
        private void Reset_Values()
        {
            cbothang.SelectedIndex = -1;
            cboquy.SelectedIndex = -1;
            txtnam.Text = "";
            cbothang.Enabled = true;
            cboquy.Enabled = true;
            txtnam.Enabled = true;
        }
        private void Load_DataGridView()
        {
            string sql;
            sql = "SELECT * FROM tblphong";
            tblcpbt = Functions.Getdatatotable(sql);
            dgridcpbt.DataSource = tblcpbt;
            dgridcpbt.Columns[0].HeaderText = "Mã phòng";
            dgridcpbt.Columns[1].HeaderText = "Tổng chi phí";
            dgridcpbt.AllowUserToAddRows = false;
            dgridcpbt.EditMode = DataGridViewEditMode.EditProgrammatically;
        }

        private void btnbaocao_Click(object sender, EventArgs e)
        {
            string sql;
            if ((cbothang.Text != "") && (txtnam.Text == ""))
            {
                MessageBox.Show("Nhập năm đi!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if ((cboquy.Text != "") && (txtnam.Text == ""))
            {
                MessageBox.Show("Nhập năm đi!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            sql = "SELECT c.Maphong, sum(a.Tongchiphi) AS Tong FROM tblbaotri a JOIN tblctbt b ON a.Mabaotri=b.Mabaotri JOIN tblmaytinh c ON c.Mamay= b.Mamay WHERE 1=1";
            if ((txtnam.Text != ""))
            {
                sql = sql + "AND (SELECT YEAR(a.Ngaybaotri) AS CHAR)='" + txtnam.Text.Trim() + "'";
            }
            if ((cbothang.Text != ""))
            {
                sql = sql + " AND (SELECT MONTH(a.Ngaybaotri) AS CHAR) ='" + cbothang.Text + "'";
            }
            if ((cboquy.Text != ""))
            {
                if (cboquy.Text == "1")
                {
                    sql = sql + " AND (SELECT MONTH(a.Ngaybaotri) AS CHAR) BETWEEN '1' AND '3'";
                }
                if (cboquy.Text == "2")
                {
                    sql = sql + " AND (SELECT MONTH(a.Ngaybaotri) AS CHAR) BETWEEN '4' AND '6'";
                }
                if (cboquy.Text == "3")
                {
                    sql = sql + " AND (SELECT MONTH(a.Ngaybaotri) AS CHAR) BETWEEN '7' AND '9'";
                }
                if (cboquy.Text == "4")
                {
                    sql = sql + " AND (SELECT MONTH(a.Ngaybaotri) AS CHAR) BETWEEN '9' AND '12'";
                }
            }
            tblcpbt = Functions.Getdatatotable(sql + "GROUP BY c.Maphong");
            if (tblcpbt.Rows.Count == 0)
            {
                MessageBox.Show("Không có dữ liệu bảo trì!!!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                Reset_Values();
                btninbaocao.Enabled = false;
                btnboqua.Enabled = false;
                btnbaocao.Enabled = true;
            }
            else
            {
                dgridcpbt.DataSource = tblcpbt;
                Load_DataGridView();
                cboquy.Enabled = false;
                cbothang.Enabled = false;
                txtnam.Enabled = false;
                btnbaocao.Enabled = false;
                btnboqua.Enabled = true;
                btninbaocao.Enabled = true;
            }
        }

        private void btnboqua_Click(object sender, EventArgs e)
        {
            Reset_Values();
            dgridcpbt.DataSource = null;
            btnbaocao.Enabled = true;
            btninbaocao.Enabled = false;
            btnboqua.Enabled = false;
        }

        private void btnthoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbothang_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboquy.Enabled = false;
        }

        private void cboquy_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbothang.Enabled = false;
        }

        private void btninbaocao_Click(object sender, EventArgs e)
        {
            // Khởi động chương trình Excel

            COMExcel.Application exApp = new COMExcel.Application();
            COMExcel.Workbook exBook; //Trong 1 chương trình Excel có nhiều Workbook
            COMExcel.Worksheet exSheet; //Trong 1 Workbook có nhiều Worksheet
            COMExcel.Range exRange;
            string sql;
            int hang = 0, cot = 0;
            DataTable ThongtinNBT, ThongtinBT;
            exBook = exApp.Workbooks.Add(COMExcel.XlWBATemplate.xlWBATWorksheet);
            exSheet = exBook.Worksheets[1];
            // Định dạng chung
            exRange = exSheet.Cells[1, 1];
            exRange.Range["A1:B3"].Font.Size = 10;
            exRange.Range["A1:B3"].Font.Name = "Calibri";
            exRange.Range["A1:B3"].Font.Bold = true;
            exRange.Range["A1:B3"].Font.ColorIndex = 5; //Màu xanh da trời
            exRange.Range["A1:A1"].ColumnWidth = 7;
            exRange.Range["B1:B1"].ColumnWidth = 15;
            exRange.Range["A1:B1"].MergeCells = true;
            exRange.Range["A1:B1"].HorizontalAlignment = COMExcel.XlHAlign.xlHAlignCenter;
            exRange.Range["A1:B1"].Value = "Quán Internet - CYBER10)";
            exRange.Range["A2:B2"].MergeCells = true;
            exRange.Range["A2:B2"].HorizontalAlignment = COMExcel.XlHAlign.xlHAlignCenter;
            exRange.Range["A2:B2"].Value = "Banking Academy";
            exRange.Range["A3:B3"].MergeCells = true;
            exRange.Range["A3:B3"].HorizontalAlignment = COMExcel.XlHAlign.xlHAlignCenter;
            exRange.Range["A3:B3"].Value = "Contact: (08)68686868";
            exRange.Range["C2:E2"].Font.Size = 16;
            exRange.Range["C2:E2"].Font.Name = "Calibri";
            exRange.Range["C2:E2"].Font.Bold = true;
            exRange.Range["C2:E2"].Font.ColorIndex = 3; //Màu đỏ
            exRange.Range["C2:E2"].MergeCells = true;
            exRange.Range["C2:E2"].HorizontalAlignment = COMExcel.XlHAlign.xlHAlignCenter;
            exRange.Range["C2:E2"].Value = "BÁO CÁO CHI PHÍ";
            // Biểu diễn thông tin chung của Báo cáo chi phí
            sql = "SELECT a.Manhabaotri,a.TenNBT,a.Diachi,a.Dienthoai FROM tblnhabaotri a JOIN tblbaotri b ON a.Manhabaotri=b.Manhabaotri";
            ThongtinNBT = Functions.Getdatatotable(sql);
            exRange.Range["B6:C9"].Font.Size = 12;
            exRange.Range["B6:C9"].Font.Name = "Calibri";
            exRange.Range["B6:B6"].Value = "Mã nhà bảo trì:";
            exRange.Range["C6:E6"].MergeCells = true;
            exRange.Range["C6:E6"].Value = ThongtinNBT.Rows[0][0].ToString();
            exRange.Range["B7:B7"].Value = "Tên nhà bảo trì:";
            exRange.Range["C7:E7"].MergeCells = true;
            exRange.Range["C7:E7"].Value = ThongtinNBT.Rows[0][1].ToString();
            exRange.Range["B8:B8"].Value = "Địa chỉ:";
            exRange.Range["C8:J8"].MergeCells = true;
            exRange.Range["C8:E8"].Value = ThongtinNBT.Rows[0][2].ToString();
            exRange.Range["B9:B9"].Value = "Điện thoại:";
            exRange.Range["C9:E9"].MergeCells = true;
            exRange.Range["C9:E9"].Value = ThongtinNBT.Rows[0][3].ToString();
            //Lấy thông tin bảo trì
            sql = "SELECT c.Maphong, sum(a.Tongchiphi) AS Tong FROM tblbaotri a JOIN tblctbt b ON a.Mabaotri=b.Mabaotri JOIN tblmaytinh c ON c.Mamay= b.Mamay WHERE 1=1";
            if ((txtnam.Text != ""))
            {
                sql = sql + "AND (SELECT YEAR(a.Ngaybaotri) AS CHAR)='" + txtnam.Text.Trim() + "'";
            }
            if ((cbothang.Text != ""))
            {
                sql = sql + " AND (SELECT MONTH(a.Ngaybaotri) AS CHAR) ='" + cbothang.Text + "'";
            }
            if ((cboquy.Text != ""))
            {
                if (cboquy.Text == "1")
                {
                    sql = sql + " AND (SELECT MONTH(a.Ngaybaotri) AS CHAR) BETWEEN '1' AND '3'";
                }
                if (cboquy.Text == "2")
                {
                    sql = sql + " AND (SELECT MONTH(a.Ngaybaotri) AS CHAR) BETWEEN '4' AND '6'";
                }
                if (cboquy.Text == "3")
                {
                    sql = sql + " AND (SELECT MONTH(a.Ngaybaotri) AS CHAR) BETWEEN '7' AND '9'";
                }
                if (cboquy.Text == "4")
                {
                    sql = sql + " AND (SELECT MONTH(a.Ngaybaotri) AS CHAR) BETWEEN '9' AND '12'";
                }
            }
            ThongtinBT = Functions.Getdatatotable(sql + "GROUP BY c.Maphong");
            //Tạo dòng tiêu đề bảng
            exRange.Range["A11:F11"].Font.Bold = true;
            exRange.Range["A11:F11"].HorizontalAlignment = COMExcel.XlHAlign.xlHAlignCenter;
            exRange.Range["A11:F11"].ColumnWidth = 15;
            exRange.Range["A11:A11"].Value = "Mã phòng";
            exRange.Range["B11:B11"].Value = "Tổng tiền";
            for (hang = 0; hang <= ThongtinBT.Rows.Count - 1; hang++)
            {
                for (cot = 0; cot <= ThongtinBT.Columns.Count - 1; cot++)
                    //Điền thông tin hàng từ cột thứ 1, dòng 12
                    exSheet.Cells[cot + 1][hang + 12] = ThongtinBT.Rows[hang][cot].ToString();
            }
            exSheet.Name = "BÁO CÁO CHI PHÍ";
            exApp.Visible = true;
        }

        private void txtnam_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (((e.KeyChar >= '0') && (e.KeyChar <= '9')) || (Convert.ToInt32(e.KeyChar) == 8) || (Convert.ToInt32(e.KeyChar) == 13))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}

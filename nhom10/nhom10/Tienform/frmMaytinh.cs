﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nhom10.Tienform
{
    public partial class frmMaytinh : Form
    {
        DataTable tblDLMT;
        public frmMaytinh()
        {
            InitializeComponent();
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void frmMaytinh_Load(object sender, EventArgs e)
        {
            Functions.ketnoi();
            btnLuu.Enabled = false;
            btnBoqua.Enabled = false;
            txtMamay.Enabled = false;
            Functions.Fillcombo("SELECT Maphong FROM tblphong", cboMaphong, "Maphong", "Maphong");
            cboMaphong.SelectedIndex = -1;
            Functions.Fillcombo("SELECT Maocung FROM tblocung", cboMaocung, "Maocung", "Maocung");
            cboMaocung.SelectedIndex = -1;
            Functions.Fillcombo("SELECT Machip FROM tblchip", cboMachip, "Machip", "Machip");
            cboMachip.SelectedIndex = -1;
            Functions.Fillcombo("SELECT Maram FROM tblram", cboMaram, "Maram", "Maram");
            cboMaram.SelectedIndex = -1;
            Functions.Fillcombo("SELECT Mamanhinh FROM tblmanhinh", cboMamanhinh, "Mamanhinh", "Mamanhinh");
            cboMamanhinh.SelectedIndex = -1;
            Functions.Fillcombo("SELECT Machuot FROM tblchuot", cboMachuot, "Machuot", "Machuot");
            cboMachuot.SelectedIndex = -1;
            Functions.Fillcombo("SELECT Mabanphim FROM tblbanphim", cboMabanphim, "Mabanphim", "Mabannphim");
            cboMabanphim.SelectedIndex = -1;
            Functions.Fillcombo("SELECT Maloa FROM tblloa", cboMaloa, "Maloa", "Maloa");
            cboMaloa.SelectedIndex = -1;
            load_datagrid();
            resetvalues();
        }
        private void load_datagrid()
        {
            string sql;
            sql = "select Mamay,Tenmay,Maphong,Maocung,Machip,Maram,Mamanhinh,Machuot,Mabanphim,Maloa,Tinhtrang,Ghichu from tblmaytinh";
            tblDLMT = Functions.Getdatatotable(sql);
            dgridDLMT.DataSource = tblDLMT;
            dgridDLMT.Columns[0].HeaderText = "Mã máy";
            dgridDLMT.Columns[1].HeaderText = "Tên máy";
            dgridDLMT.Columns[2].HeaderText = "Mã phòng";
            dgridDLMT.Columns[3].HeaderText = "Mã ổ cứng";
            dgridDLMT.Columns[4].HeaderText = "Mã chip";
            dgridDLMT.Columns[5].HeaderText = "Mã ram";
            dgridDLMT.Columns[6].HeaderText = "Mã màn hình";
            dgridDLMT.Columns[7].HeaderText = "Mã chuột";
            dgridDLMT.Columns[8].HeaderText = "Mã bàn phím";
            dgridDLMT.Columns[9].HeaderText = "Mã loa";
            dgridDLMT.Columns[10].HeaderText = "Tình trạng";
            dgridDLMT.AllowUserToAddRows = false;
            dgridDLMT.EditMode = DataGridViewEditMode.EditProgrammatically;

        }

        private void txtMamay_TextChanged(object sender, EventArgs e)
        {

        }

        private void dgridDLMT_Click(object sender, EventArgs e)
        {
            string p, oc, c, r, m, ch, L, bp;
            if (btnThem.Enabled == false)
            {
                MessageBox.Show("Đang ở chế độ thêm mới!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMamay.Focus();
                return;
            }
            if (tblDLMT.Rows.Count == 0)
            {
                MessageBox.Show("Không có dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            txtMamay.Text = dgridDLMT.CurrentRow.Cells["Mamay"].Value.ToString();
            
            
            p = dgridDLMT.CurrentRow.Cells["Maphong"].Value.ToString();
            cboMaphong.Text = Functions.GetFieldValues("SELECT Maphong FROM tblphong WHERE Maphong =N'" + p + "'");
            oc = dgridDLMT.CurrentRow.Cells["Maocung"].Value.ToString();
            cboMaocung.Text = Functions.GetFieldValues("SELECT Maocung FROM tblocung WHERE Maocung =N'" + oc + "'");
            c = dgridDLMT.CurrentRow.Cells["Machip"].Value.ToString();
            cboMachip.Text = Functions.GetFieldValues("SELECT Machip FROM tblchip WHERE Machip =N'" + c + "'");
            r = dgridDLMT.CurrentRow.Cells["Maram"].Value.ToString();
            cboMaram.Text = Functions.GetFieldValues("SELECT Maram FROM tblram WHERE Maram =N'" + r + "'");
            m = dgridDLMT.CurrentRow.Cells["Mamanhinh"].Value.ToString();
            cboMamanhinh.Text = Functions.GetFieldValues("SELECT Mamanhinh FROM tblmanhinh WHERE Mamanhinh =N'" + m + "'");
            L = dgridDLMT.CurrentRow.Cells["Maloa"].Value.ToString();
            cboMaloa.Text = Functions.GetFieldValues("SELECT Maloa FROM tblloa WHERE Maloa =N'" + L + "'");
            ch = dgridDLMT.CurrentRow.Cells["Machuot"].Value.ToString();
            cboMachuot.Text = Functions.GetFieldValues("SELECT Machuot FROM tblchuot WHERE Machuot =N'" + ch + "'");
            bp = dgridDLMT.CurrentRow.Cells["Mabanphim"].Value.ToString();
            cboMabanphim.Text = Functions.GetFieldValues("SELECT Mabanphim FROM tblbanphim WHERE Mabanphim =N'" + bp + "'");
           
            txtTinhtrang.Text = dgridDLMT.CurrentRow.Cells["Tinhtrang"].Value.ToString();
            txtGhichu.Text = dgridDLMT.CurrentRow.Cells["Ghichu"].Value.ToString();
            btnBoqua.Enabled = true;
            btnSua.Enabled = true;
            btnXoa.Enabled = true;

        }
        private void resetvalues()
        {
            txtMamay.Text = "";
            txttenmay.Text = "";
            txtGhichu.Text = "";
            txtTinhtrang.Text = "";
            cboMabanphim.Text = "";
            cboMachip.Text = "";
            cboMamanhinh.Text = "";
            cboMamanhinh.Text = "";
            cboMaocung.Text = "";
            cboMaphong.Text = "";
            cboMaram.Text = "";
            
          
            cboMaloa.Text = "";
           
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            btnThem.Enabled = false;
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
            btnLuu.Enabled = true;
            btnBoqua.Enabled = true;
            resetvalues();
            txtMamay.Focus();
            txtMamay.Enabled = true;
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            string sql;
            double soluong, slmoi;
            if (txtMamay.Text == "")
            {
                MessageBox.Show("B phải nhập mã máy", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtMamay.Focus();
                return;
            }
            if (txttenmay.Text == "")
            {
                MessageBox.Show("B phải nhập tên máy", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txttenmay.Focus();
                return;
            }
            if (cboMaphong.Text.Trim().Length == 0)
            {
                MessageBox.Show("B phải nhập mã phòng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboMaphong.Focus();
                return;
            }
            if (cboMachip.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mã chip", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboMachip.Focus();
                return;
            }
            if (cboMaocung.Text.Trim().Length == 0)
            {
                MessageBox.Show("B phải nhập mã ổ cứng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboMaocung.Focus();
                return;
            }


            if (cboMaram.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mã ram", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboMaram.Focus();
                return;
            }
            if (cboMamanhinh.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mã màn hình", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboMamanhinh.Focus();
                return;
            }
            if (cboMachuot.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mã chuột", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboMachuot.Focus();
                return;
            }
            if (cboMabanphim.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mã bàn phím", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboMabanphim.Focus();
                return;
            }
            if (cboMaloa.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mã loa", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboMaloa.Focus();
                return;
            }
            
            
            if (txtGhichu.Text == "")
            {
                MessageBox.Show("Bạn phải nhập ghi chú", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtGhichu.Focus();
                return;
            }
            if (txtTinhtrang.Text == "")
            {
                MessageBox.Show("Bạn phải nhập tình trạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtTinhtrang.Focus();
                return;
            }
            sql = "SELECT Mamay FROM tblmaytinh WHERE Mamay=N'" + txtMamay.Text.Trim() + "'";
            if (Functions.CheckKey(sql))
            {
                MessageBox.Show("mã máy này đã có, bạn phải nhập mã khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtMamay.Focus();
                txtMamay.Text = "";
                return;
            }
            sql = "INSERT INTO tblmaytinh(Mamay,Tenmay,Maphong,Maocung,Machip,Maram,Mamanhinh,Machuot,Mabanphim,Maloa,Tinhtrang,Ghichu) VALUES(N'" + txtMamay.Text.Trim() + "',N'" + txttenmay.Text.Trim() + "',N'" + cboMaphong.SelectedValue.ToString() + "',N'" + cboMaocung.SelectedValue.ToString() + "',N'" + cboMachip.SelectedValue.ToString() + "',N'" + cboMaram.SelectedValue.ToString() + "',N'" + cboMamanhinh.SelectedValue.ToString() + "',N'" + cboMachuot.SelectedValue.ToString() + "',N'" + cboMabanphim.SelectedValue.ToString() + "',N'" + cboMaloa.SelectedValue.ToString() + "',N'" + txtTinhtrang.Text.Trim() + "',N'" + txtGhichu.Text.Trim() + "')";
            Functions.RunSql(sql);
            load_datagrid();

            soluong = Convert.ToDouble(Functions.GetFieldValues("SELECT Somay FROM tblphong WHERE Maphong=N'" + cboMaphong.SelectedValue + "'"));
            slmoi = soluong + 1;
            sql = "UPDATE tblphong SET Somay=" + slmoi + "WHERE Maphong =N'" + cboMaphong.SelectedValue + "'";
            Functions.RunSql(sql);
            resetvalues();
            btnXoa.Enabled = true;
            btnSua.Enabled = true;
            btnLuu.Enabled = false;
            btnBoqua.Enabled = false;
            btnThem.Enabled = true;
            txtMamay.Enabled = false;

        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            string sql, sql1, sql2;
            int a, slmoi, soluong;
            if (txtMamay.Text == "")
            {
                MessageBox.Show("Bạn phải nhập mã máy", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtMamay.Focus();
                return;
            }
            if (cboMaphong.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mã phòng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboMaphong.Focus();
                return;
            }
            
           
           
            if (cboMaocung.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mã ổ cứng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboMaocung.Focus();
                return;
            }
            if (cboMachip.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mã chip", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboMachip.Focus();
                return;
            }
            if (cboMaram.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mã ram", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboMaram.Focus();
                return;
            }
            if (cboMamanhinh.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mã màn hình", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboMamanhinh.Focus();
                return;
            }
            if (cboMachuot.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mã chuột", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboMachuot.Focus();
                return;
            }
            if (cboMabanphim.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mã bàn phím", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboMabanphim.Focus();
                return;
            }
            if (cboMaloa.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mã loa", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboMaloa.Focus();
                return;
            }
            if (cboMaphong.SelectedIndex != -1)
            {
                //Trừ đi số lượng phòng cũ
                a = Convert.ToInt32(Functions.GetFieldValues("SELECT a.Somay FROM tblphong a JOIN tblmaytinh b ON a.Maphong=b.Maphong WHERE Mamay=N'" + txtMamay.Text.Trim() + "'"));
                a = a - 1;
                string phongcu = Functions.GetFieldValues("SELECT Maphong FROM tblmaytinh WHERE Mamay='" + txtMamay.Text.Trim() + "'");
                sql1 = "UPDATE tblphong SET Somay=" + a + "WHERE Maphong='" + phongcu + "'";
                Functions.RunSql(sql1);

                soluong = Convert.ToInt32(Functions.GetFieldValues("SELECT Somay FROM tblphong WHERE Maphong=N'" + cboMaphong.SelectedValue + "'"));
                slmoi = soluong + 1;
                sql2 = "UPDATE tblphong SET Somay=" + slmoi + "WHERE Maphong =N'" + cboMaphong.SelectedValue + "'";
                Functions.RunSql(sql2);

                sql = "UPDATE tblmaytinh SET Maphong=N'" + cboMaphong.Text + "'WHERE Mamay=N'" + txtMamay.Text + "'";
                Functions.RunSql(sql);
            }
            
            
            if (cboMaocung.SelectedIndex != -1)
            {
                sql = "UPDATE tblmaytinh SET Maoocung=N'" + cboMaocung.SelectedValue.ToString() + "'WHERE Mamay=N'" + txtMamay.Text + "'";
                Functions.RunSql(sql);
            }
            if (cboMachip.SelectedIndex != -1)
            {
                sql = "UPDATE tblmaytinh SET Machip=N'" + cboMachip.SelectedValue.ToString() + "'WHERE Mamay=N'" + txtMamay.Text + "'";
                Functions.RunSql(sql);
            }
            if (cboMaram.SelectedIndex != -1)
            {
                sql = "UPDATE tblmaytinh SET Maram=N'" + cboMaram.SelectedValue.ToString() + "'WHERE Mamay=N'" + txtMamay.Text + "'";
                Functions.RunSql(sql);
            }
            if (cboMamanhinh.SelectedIndex != -1)
            {
                sql = "UPDATE tblmaytinh SET Mamanhinh=N'" + cboMamanhinh.SelectedValue.ToString() + "'WHERE Mamay=N'" + txtMamay.Text + "'";
                Functions.RunSql(sql);
            }
            if (cboMaloa.SelectedIndex != -1)
            {
                sql = "UPDATE tblmaytinh SET Maloa=N'" + cboMaloa.SelectedValue.ToString() + "'WHERE Mamay=N'" + txtMamay.Text + "'";
                Functions.RunSql(sql);
            }
            if (cboMachuot.SelectedIndex != -1)
            {
                sql = "UPDATE tblmaytinh SET Machuot=N'" + cboMachuot.SelectedValue.ToString() + "'WHERE Mamay=N'" + txtMamay.Text + "'";
                Functions.RunSql(sql);
            }
            if (cboMabanphim.SelectedIndex != -1)
            {
                sql = "UPDATE tblmaytinh SET Mabanphim=N'" + cboMabanphim.SelectedValue.ToString() + "'WHERE Mamay=N'" + txtMamay.Text + "'";
                Functions.RunSql(sql);
            }
            if (txtTinhtrang.Text.Trim() != "")
            {
                sql = "UPDATE tblmaytinh SET Tinhtrang=N'" + txtTinhtrang.Text.Trim() + "'WHERE Mamay=N'" + txtMamay.Text + "'";
                Functions.RunSql(sql);
            }
            if (txtGhichu.Text.Trim() != "")
            {
                sql = "UPDATE tblmaytinh SET Ghichu=N'" + txtGhichu.Text.Trim() + "'WHERE Mamay=N'" + txtMamay.Text + "'";
                Functions.RunSql(sql);
            }
            load_datagrid();
            resetvalues();
            btnBoqua.Enabled = false;
        }

        private void btnboqua_Click(object sender, EventArgs e)
        {
            resetvalues();
            btnBoqua.Enabled = false;
            btnThem.Enabled = true;
            btnXoa.Enabled = true;
            btnSua.Enabled = true;
            btnLuu.Enabled = false;
            txtMamay.Enabled = false;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}



        
    


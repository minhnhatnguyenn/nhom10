﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nhom10.Tienform
{
    
    public partial class frmChip : Form
    {
        DataTable Chip;
        public frmChip()
        {
            InitializeComponent();
        }

        private void frmChip_Load(object sender, EventArgs e)
        {
            Functions.ketnoi();
            txtMachip.Enabled = false;
            btnLuu.Enabled = false;
            btnBoqua.Enabled = false;
            Load_DataGridView();

        }
        private void Load_DataGridView()
        {
            string sql;
            sql = "SELECT Machip,Tenchip FROM tblchip";
            Chip = Functions.Getdatatotable(sql);
            dgridC.DataSource = Chip;

            dgridC.Columns[0].HeaderText = "Mã chip";
            dgridC.Columns[1].HeaderText = "Tên chip";

            dgridC.AllowUserToAddRows = false;
            // Không cho phép sửa dữ liệu trực tiếp trên lưới
            dgridC.EditMode = DataGridViewEditMode.EditProgrammatically;
        }

        private void dgridC_Click(object sender, EventArgs e)
        {
            if (btnThem.Enabled == false)
            {
                MessageBox.Show("Đang ở chế độ thêm mới!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMachip.Focus();
                return;
            }
            if (Chip.Rows.Count == 0)
            {
                MessageBox.Show("Không có dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            txtMachip.Text = dgridC.CurrentRow.Cells["Machip"].Value.ToString();
            txtTenchip.Text = dgridC.CurrentRow.Cells["Tenchip"].Value.ToString();

            btnSua.Enabled = true;
            btnXoa.Enabled = true;
            btnBoqua.Enabled = true;
        }
        private void ResetValues()
        {
            txtMachip.Text = "";
            txtTenchip.Text = "";

        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
            btnBoqua.Enabled = true;
            btnLuu.Enabled = true;
            btnThem.Enabled = false;
            ResetValues();
            txtMachip.Enabled = true;
            txtMachip.Focus();

        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            string sql;
            if (txtMachip.Text == "")
            {
                MessageBox.Show("Bạn phải nhập mã chip ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtMachip.Focus();
                return;
            }
            if (txtTenchip.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên chip", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtTenchip.Focus();
                return;
            }

            sql = "SELECT Machip FROM tblchip WHERE Machip=N'" + txtMachip.Text.Trim() + "'";
            if (Functions.CheckKey(sql))
            {
                MessageBox.Show("Mã chip này đã có, bạn phải nhập mã khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtMachip.Focus();
                txtMachip.Text = "";
                return;
            }
            sql = "INSERT INTO tblchip(Machip,Tenchip) VALUES(N'" + txtMachip.Text + "',N'" + txtTenchip.Text + "')";
            Functions.RunSql(sql);
            Load_DataGridView();
            ResetValues();
            btnXoa.Enabled = true;
            btnThem.Enabled = true;
            btnSua.Enabled = true;
            btnBoqua.Enabled = false;
            btnLuu.Enabled = false;
            txtMachip.Enabled = false;
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            string sql;
            if (Chip.Rows.Count == 0)
            {
                MessageBox.Show("Không còn dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtMachip.Text == "")
            {
                MessageBox.Show("Bạn chưa chọn bản ghi nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtTenchip.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên chip", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtTenchip.Focus();
                return;
            }

            sql = "UPDATE tblchip SET  Tenchip=N'" + txtTenchip.Text.Trim().ToString()
                   + "' WHERE Machip=N'" + txtMachip.Text + "'";
            Functions.RunSql(sql);
            Load_DataGridView();
            ResetValues();
            btnBoqua.Enabled = false;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            string sql;
            if (Chip.Rows.Count == 0)
            {
                MessageBox.Show("Không còn dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtMachip.Text == "")
            {
                MessageBox.Show("Bạn chưa chọn bản ghi nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (MessageBox.Show("Bạn có muốn xóa không?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                sql = "DELETE tblchip WHERE Machip=N'" + txtMachip.Text + "'";
                Functions.RunSqlDel(sql);
                Load_DataGridView();
                ResetValues();
            }
        }

        private void btnBoqua_Click(object sender, EventArgs e)
        {
            ResetValues();
            btnBoqua.Enabled = false;
            btnThem.Enabled = true;
            btnXoa.Enabled = true;
            btnSua.Enabled = true;
            btnLuu.Enabled = false;
            txtMachip.Enabled = false;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}


﻿
namespace nhom10.Tienform
{
    partial class frmMaytinh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDong = new System.Windows.Forms.Button();
            this.btnBoqua = new System.Windows.Forms.Button();
            this.btnLuu = new System.Windows.Forms.Button();
            this.btnSua = new System.Windows.Forms.Button();
            this.btnXoa = new System.Windows.Forms.Button();
            this.btnThem = new System.Windows.Forms.Button();
            this.dgridDLMT = new System.Windows.Forms.DataGridView();
            this.cboMaloa = new System.Windows.Forms.ComboBox();
            this.cboMabanphim = new System.Windows.Forms.ComboBox();
            this.cboMachuot = new System.Windows.Forms.ComboBox();
            this.cboMamanhinh = new System.Windows.Forms.ComboBox();
            this.cboMaram = new System.Windows.Forms.ComboBox();
            this.cboMachip = new System.Windows.Forms.ComboBox();
            this.cboMaocung = new System.Windows.Forms.ComboBox();
            this.cboMaphong = new System.Windows.Forms.ComboBox();
            this.txtGhichu = new System.Windows.Forms.TextBox();
            this.txtMamay = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTinhtrang = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txttenmay = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgridDLMT)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDong
            // 
            this.btnDong.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Location = new System.Drawing.Point(955, 304);
            this.btnDong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(100, 28);
            this.btnDong.TabIndex = 103;
            this.btnDong.Text = "Đóng";
            this.btnDong.UseVisualStyleBackColor = true;
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnBoqua
            // 
            this.btnBoqua.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBoqua.Location = new System.Drawing.Point(955, 251);
            this.btnBoqua.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnBoqua.Name = "btnBoqua";
            this.btnBoqua.Size = new System.Drawing.Size(100, 28);
            this.btnBoqua.TabIndex = 102;
            this.btnBoqua.Text = "Bỏ qua";
            this.btnBoqua.UseVisualStyleBackColor = true;
            // 
            // btnLuu
            // 
            this.btnLuu.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Location = new System.Drawing.Point(955, 92);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(100, 28);
            this.btnLuu.TabIndex = 101;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.UseVisualStyleBackColor = true;
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnSua
            // 
            this.btnSua.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua.Location = new System.Drawing.Point(955, 146);
            this.btnSua.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(100, 28);
            this.btnSua.TabIndex = 100;
            this.btnSua.Text = "Sửa";
            this.btnSua.UseVisualStyleBackColor = true;
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Location = new System.Drawing.Point(955, 201);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(100, 28);
            this.btnXoa.TabIndex = 99;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.UseVisualStyleBackColor = true;
            // 
            // btnThem
            // 
            this.btnThem.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Location = new System.Drawing.Point(955, 41);
            this.btnThem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(100, 28);
            this.btnThem.TabIndex = 98;
            this.btnThem.Text = "Thêm";
            this.btnThem.UseVisualStyleBackColor = true;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // dgridDLMT
            // 
            this.dgridDLMT.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgridDLMT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgridDLMT.Location = new System.Drawing.Point(13, 415);
            this.dgridDLMT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgridDLMT.Name = "dgridDLMT";
            this.dgridDLMT.RowHeadersWidth = 51;
            this.dgridDLMT.RowTemplate.Height = 24;
            this.dgridDLMT.Size = new System.Drawing.Size(924, 188);
            this.dgridDLMT.TabIndex = 97;
            this.dgridDLMT.Click += new System.EventHandler(this.dgridDLMT_Click);
            // 
            // cboMaloa
            // 
            this.cboMaloa.FormattingEnabled = true;
            this.cboMaloa.Location = new System.Drawing.Point(645, 175);
            this.cboMaloa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cboMaloa.Name = "cboMaloa";
            this.cboMaloa.Size = new System.Drawing.Size(252, 25);
            this.cboMaloa.TabIndex = 95;
            // 
            // cboMabanphim
            // 
            this.cboMabanphim.FormattingEnabled = true;
            this.cboMabanphim.Location = new System.Drawing.Point(645, 123);
            this.cboMabanphim.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cboMabanphim.Name = "cboMabanphim";
            this.cboMabanphim.Size = new System.Drawing.Size(252, 25);
            this.cboMabanphim.TabIndex = 94;
            // 
            // cboMachuot
            // 
            this.cboMachuot.FormattingEnabled = true;
            this.cboMachuot.Location = new System.Drawing.Point(645, 74);
            this.cboMachuot.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cboMachuot.Name = "cboMachuot";
            this.cboMachuot.Size = new System.Drawing.Size(252, 25);
            this.cboMachuot.TabIndex = 93;
            // 
            // cboMamanhinh
            // 
            this.cboMamanhinh.FormattingEnabled = true;
            this.cboMamanhinh.Location = new System.Drawing.Point(645, 32);
            this.cboMamanhinh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cboMamanhinh.Name = "cboMamanhinh";
            this.cboMamanhinh.Size = new System.Drawing.Size(252, 25);
            this.cboMamanhinh.TabIndex = 92;
            // 
            // cboMaram
            // 
            this.cboMaram.FormattingEnabled = true;
            this.cboMaram.Location = new System.Drawing.Point(149, 271);
            this.cboMaram.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cboMaram.Name = "cboMaram";
            this.cboMaram.Size = new System.Drawing.Size(252, 25);
            this.cboMaram.TabIndex = 91;
            // 
            // cboMachip
            // 
            this.cboMachip.FormattingEnabled = true;
            this.cboMachip.Location = new System.Drawing.Point(149, 224);
            this.cboMachip.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cboMachip.Name = "cboMachip";
            this.cboMachip.Size = new System.Drawing.Size(252, 25);
            this.cboMachip.TabIndex = 90;
            // 
            // cboMaocung
            // 
            this.cboMaocung.FormattingEnabled = true;
            this.cboMaocung.Location = new System.Drawing.Point(149, 171);
            this.cboMaocung.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cboMaocung.Name = "cboMaocung";
            this.cboMaocung.Size = new System.Drawing.Size(252, 25);
            this.cboMaocung.TabIndex = 96;
            // 
            // cboMaphong
            // 
            this.cboMaphong.FormattingEnabled = true;
            this.cboMaphong.Location = new System.Drawing.Point(149, 119);
            this.cboMaphong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cboMaphong.Name = "cboMaphong";
            this.cboMaphong.Size = new System.Drawing.Size(252, 25);
            this.cboMaphong.TabIndex = 89;
            // 
            // txtGhichu
            // 
            this.txtGhichu.Location = new System.Drawing.Point(645, 271);
            this.txtGhichu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtGhichu.Name = "txtGhichu";
            this.txtGhichu.Size = new System.Drawing.Size(252, 25);
            this.txtGhichu.TabIndex = 88;
            // 
            // txtMamay
            // 
            this.txtMamay.Location = new System.Drawing.Point(149, 32);
            this.txtMamay.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtMamay.Name = "txtMamay";
            this.txtMamay.Size = new System.Drawing.Size(252, 25);
            this.txtMamay.TabIndex = 87;
            this.txtMamay.TextChanged += new System.EventHandler(this.txtMamay_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.GhostWhite;
            this.label16.Location = new System.Drawing.Point(576, 271);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(63, 21);
            this.label16.TabIndex = 86;
            this.label16.Text = "Ghi chú";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.GhostWhite;
            this.label15.Location = new System.Drawing.Point(558, 224);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(81, 21);
            this.label15.TabIndex = 85;
            this.label15.Text = "Tình trạng";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.GhostWhite;
            this.label14.Location = new System.Drawing.Point(597, 175);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 21);
            this.label14.TabIndex = 84;
            this.label14.Text = "Loa";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.GhostWhite;
            this.label13.Location = new System.Drawing.Point(557, 123);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 21);
            this.label13.TabIndex = 83;
            this.label13.Text = "Bàn Phím";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.GhostWhite;
            this.label9.Location = new System.Drawing.Point(579, 74);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 21);
            this.label9.TabIndex = 82;
            this.label9.Text = "Chuột";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.GhostWhite;
            this.label8.Location = new System.Drawing.Point(557, 36);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 21);
            this.label8.TabIndex = 81;
            this.label8.Text = "Màn hình";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.GhostWhite;
            this.label7.Location = new System.Drawing.Point(97, 271);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 21);
            this.label7.TabIndex = 80;
            this.label7.Text = "Ram";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.GhostWhite;
            this.label6.Location = new System.Drawing.Point(98, 228);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 21);
            this.label6.TabIndex = 79;
            this.label6.Text = "Chip";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.GhostWhite;
            this.label4.Location = new System.Drawing.Point(76, 171);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 21);
            this.label4.TabIndex = 78;
            this.label4.Text = "Ổ Cứng";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.GhostWhite;
            this.label3.Location = new System.Drawing.Point(85, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 21);
            this.label3.TabIndex = 77;
            this.label3.Text = "Phòng";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.GhostWhite;
            this.label1.Location = new System.Drawing.Point(76, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 21);
            this.label1.TabIndex = 76;
            this.label1.Text = "Mã máy";
            // 
            // txtTinhtrang
            // 
            this.txtTinhtrang.Location = new System.Drawing.Point(645, 220);
            this.txtTinhtrang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTinhtrang.Name = "txtTinhtrang";
            this.txtTinhtrang.Size = new System.Drawing.Size(252, 25);
            this.txtTinhtrang.TabIndex = 105;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.groupBox1.Controls.Add(this.txttenmay);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtTinhtrang);
            this.groupBox1.Controls.Add(this.txtMamay);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cboMaphong);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cboMaocung);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.txtGhichu);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.cboMachip);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.cboMaloa);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.cboMabanphim);
            this.groupBox1.Controls.Add(this.cboMaram);
            this.groupBox1.Controls.Add(this.cboMachuot);
            this.groupBox1.Controls.Add(this.cboMamanhinh);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Location = new System.Drawing.Point(13, 51);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(924, 358);
            this.groupBox1.TabIndex = 106;
            this.groupBox1.TabStop = false;
            // 
            // txttenmay
            // 
            this.txttenmay.Location = new System.Drawing.Point(149, 70);
            this.txttenmay.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txttenmay.Name = "txttenmay";
            this.txttenmay.Size = new System.Drawing.Size(252, 25);
            this.txttenmay.TabIndex = 115;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.GhostWhite;
            this.label12.Location = new System.Drawing.Point(70, 70);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 21);
            this.label12.TabIndex = 114;
            this.label12.Text = "Tên máy";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(335, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(200, 29);
            this.label2.TabIndex = 107;
            this.label2.Text = "DỮ LIỆU MÁY TÍNH";
            // 
            // frmMaytinh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.ClientSize = new System.Drawing.Size(1107, 746);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.btnBoqua);
            this.Controls.Add(this.btnLuu);
            this.Controls.Add(this.btnSua);
            this.Controls.Add(this.btnXoa);
            this.Controls.Add(this.btnThem);
            this.Controls.Add(this.dgridDLMT);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmMaytinh";
            this.Text = "Dữ liệu máy tính";
            this.Load += new System.EventHandler(this.frmMaytinh_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgridDLMT)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnDong;
        private System.Windows.Forms.Button btnBoqua;
        private System.Windows.Forms.Button btnLuu;
        private System.Windows.Forms.Button btnSua;
        private System.Windows.Forms.Button btnXoa;
        private System.Windows.Forms.Button btnThem;
        private System.Windows.Forms.DataGridView dgridDLMT;
        private System.Windows.Forms.ComboBox cboMaloa;
        private System.Windows.Forms.ComboBox cboMabanphim;
        private System.Windows.Forms.ComboBox cboMachuot;
        private System.Windows.Forms.ComboBox cboMamanhinh;
        private System.Windows.Forms.ComboBox cboMaram;
        private System.Windows.Forms.ComboBox cboMachip;
        private System.Windows.Forms.ComboBox cboMaocung;
        private System.Windows.Forms.ComboBox cboMaphong;
        private System.Windows.Forms.TextBox txtGhichu;
        private System.Windows.Forms.TextBox txtMamay;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTinhtrang;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txttenmay;
        private System.Windows.Forms.Label label2;
    }


}






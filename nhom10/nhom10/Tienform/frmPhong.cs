﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nhom10.Tienform
{
    public partial class frmPhong : Form
    {
        DataTable Phg;
        public frmPhong()
        {
            InitializeComponent();
        }

        private void frmPhong_Load(object sender, EventArgs e)
        
            {
                Functions.ketnoi();
                txtMaphong.Enabled = false;
                btnLuu.Enabled = false;
                btnBoqua.Enabled = false;
                Load_DataGridView();

            }
            private void Load_DataGridView()
            {
                string sql;
                sql = "SELECT * FROM tblphong";
                Phg = Functions.Getdatatotable(sql);
                dgridDLP.DataSource = Phg;
                dgridDLP.Columns[0].HeaderText = "Mã phòng";
                dgridDLP.Columns[1].HeaderText = "Số máy";
                dgridDLP.Columns[2].HeaderText = "Tên Phòng ";

                // Không cho phép thêm mới dữ liệu trực tiếp trên lưới
                dgridDLP.AllowUserToAddRows = false;
                // Không cho phép sửa dữ liệu trực tiếp trên lưới
                dgridDLP.EditMode = DataGridViewEditMode.EditProgrammatically;
            }

            private void dgridDLP_Click(object sender, EventArgs e)
            {
                if (btnThem.Enabled == false)
                {
                    MessageBox.Show("Đang ở chế độ thêm mới!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtMaphong.Focus();
                    return;
                }
                if (Phg.Rows.Count == 0)
                {
                    MessageBox.Show("Không có dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                txtMaphong.Text = dgridDLP.CurrentRow.Cells["Maphong"].Value.ToString();
                txtSomay.Text = dgridDLP.CurrentRow.Cells["Somay"].Value.ToString();
                txtTenphong.Text = dgridDLP.CurrentRow.Cells["Tenphong"].Value.ToString();
                btnSua.Enabled = true;
                btnXoa.Enabled = true;
                btnBoqua.Enabled = true;
            }
            private void ResetValues()
            {
                txtMaphong.Text = "";
                txtSomay.Text = "";
                txtTenphong.Text = "";
            }

            private void btnThem_Click(object sender, EventArgs e)
            {
                btnSua.Enabled = false;
                btnXoa.Enabled = false;
                btnBoqua.Enabled = true;
                btnLuu.Enabled = true;
                btnThem.Enabled = false;
                ResetValues();
                txtMaphong.Enabled = true;
                txtMaphong.Focus();
            }

            private void btnLuu_Click(object sender, EventArgs e)
            {
                string sql;
                if (txtMaphong.Text == "")
                {
                    MessageBox.Show("Bạn phải nhập mã phòng ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtMaphong.Focus();
                    return;
                }
                if (txtSomay.Text.Trim().Length == 0)
                {
                    MessageBox.Show("Bạn phải nhập số máy", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtSomay.Focus();
                    return;
                }
                if (txtTenphong.Text.Trim().Length == 0)
                {
                    MessageBox.Show("Bạn phải nhập tên phòng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtTenphong.Focus();
                    return;
                }
                sql = "SELECT Maphong FROM tblphong WHERE Maphong=N'" + txtMaphong.Text.Trim() + "'";
                if (Functions.CheckKey(sql))
                {
                    MessageBox.Show("Mã phòng này đã có, bạn phải nhập mã khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtMaphong.Focus();
                    txtMaphong.Text = "";
                    return;
                }
                sql = "INSERT INTO tblphong(Maphong,Somay,Tenphong) VALUES(N'" + txtMaphong.Text + "',N'" + txtSomay.Text + "',N'" + txtTenphong.Text + "')";
                Functions.RunSql(sql);
                Load_DataGridView();
                ResetValues();
                btnXoa.Enabled = true;
                btnThem.Enabled = true;
                btnSua.Enabled = true;
                btnBoqua.Enabled = false;
                btnLuu.Enabled = false;
                txtMaphong.Enabled = false;
            }

            private void btnXoa_Click(object sender, EventArgs e)
            {
                string sql;
                if (Phg.Rows.Count == 0)
                {
                    MessageBox.Show("Không còn dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (txtMaphong.Text == "")
                {
                    MessageBox.Show("Bạn chưa chọn bản ghi nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (MessageBox.Show("Bạn có muốn xóa không?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    sql = "DELETE tblphong WHERE Maphong=N'" + txtMaphong.Text + "'";
                    Functions.RunSqlDel(sql);
                    Load_DataGridView();
                    ResetValues();
                }
            }

            private void btnSua_Click(object sender, EventArgs e)
            {
                string sql;
                if (Phg.Rows.Count == 0)
                {
                    MessageBox.Show("Không còn dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (txtMaphong.Text == "")
                {
                    MessageBox.Show("Bạn chưa chọn bản ghi nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (txtSomay.Text.Trim().Length == 0)
                {
                    MessageBox.Show("Bạn phải nhập số máy ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtSomay.Focus();
                    return;
                }
                if (txtTenphong.Text.Trim().Length == 0)
                {
                    MessageBox.Show("Bạn phải nhập tên phòng ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtTenphong.Focus();
                    return;
                }
                sql = "UPDATE tblphong SET  SoMay=N'" + txtSomay.Text.Trim().ToString()
                      + "',Tenphong=N'" + txtTenphong.Text.Trim().ToString() + "' WHERE Maphong=N'" + txtMaphong.Text + "'";
                Functions.RunSql(sql);
                Load_DataGridView();
                ResetValues();
                btnBoqua.Enabled = false;
            }

            private void btnBoqua_Click(object sender, EventArgs e)
            {
                ResetValues();
                btnBoqua.Enabled = false;
                btnThem.Enabled = true;
                btnXoa.Enabled = true;
                btnSua.Enabled = true;
                btnLuu.Enabled = false;
                txtMaphong.Enabled = false;
            }

            private void btnDong_Click(object sender, EventArgs e)
            {
                this.Close();
            }
        }

    }


